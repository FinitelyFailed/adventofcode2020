package com.example.adventofcode2020

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.example.adventofcode2020.advent.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.example.adventofcode2020.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    val log = {
        "hej"
    }

    var binding: ActivityMainBinding? = null

    @ExperimentalUnsignedTypes
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        CoroutineScope(Dispatchers.Default).launch {
            runAdventOfCode()
        }
    }

    private fun runAdventOfCode() {
        val days: MutableList<BaseDay> = mutableListOf(
                Day01(),
                Day02(),
                Day03(),
                Day04(),
                Day05(),
                Day06(),
                Day07(),
                Day08(),
                Day09(),
                Day10(),
                Day11(),
                Day12(),
                Day13(),
                Day14(),
                Day15(),
                Day16(),
                Day17(),
                Day18(),
                Day19(),
                Day20(),
                Day21(),
                Day22(),
                Day23(),
                Day24()
//                Day25()
        )

        days.reverse()

        days.forEachIndexed { index, day ->
            val dayNum = days.size - index
            Log.d(TAG, "TEST Day$dayNum Answer1: ${day.testAnswer01(this)}")
            Log.d(TAG, "Day$dayNum Answer1: ${day.getAnswer01(this)}")
            Log.d(TAG, "TEST Day$dayNum Answer2: ${day.testAnswer02(this)}")
            Log.d(TAG, "Day$dayNum Answer2: ${day.getAnswer02(this)}")
        }
    }

    companion object {
        private const val TAG: String = "AdventOfCode"
    }
}