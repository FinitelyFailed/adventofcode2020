package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log
import kotlin.math.sqrt
import kotlin.text.StringBuilder

class Day20: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val tiles = getTiles(context, "day20_test_1")

//        tiles.forEach { tile ->
//            Log.d(TAG, tile.toString())
//        }

        matchNeighbors(tiles)

        val cornerProduct = getCornerProduct(tiles)

        return cornerProduct == 20899048083289
    }

    override fun testAnswer02(context: Context): Boolean {
        var tiles = getTiles(context, "day20_test_1")

//        tiles.forEach { tile ->
//            Log.d(TAG, tile.toString())
//        }

        matchNeighbors(tiles)

//        Log.d(TAG, "Before rearrange of tiles:\n${toString(tiles)}")

        tiles = rearrangeTiles(tiles)

//        Log.d(TAG, "After rearrange of tiles:\n${toString(tiles)}")

        tiles = rotateTiles(tiles)

//        Log.d(TAG, "After rotating tiles:\n${toString(tiles)}")

        val greatTile = combineTilesToOne(tiles)

//        Log.d(TAG, "After combining tiles to one:\n${toString(listOf(greatTile))}")

        greatTile.findAndMarkPattern(getMonsterPattern(), 'O')

//        Log.d(TAG, "After marking monster(s):\n${toString(listOf(greatTile))}")

        val roughnessOfTheWaters = greatTile.getRoughnessOfTheWaters()

        return roughnessOfTheWaters == 273
    }

    override fun getAnswer01(context: Context): String {
        val tiles = getTiles(context, "day20")

        matchNeighbors(tiles)

        val cornerProduct = getCornerProduct(tiles)

        return cornerProduct.toString()
    }

    override fun getAnswer02(context: Context): String {
        var tiles = getTiles(context, "day20")

        matchNeighbors(tiles)

        //Log.d(TAG, "Before rearrange of tiles")

        tiles = rearrangeTiles(tiles)

        //print("After rearrange of tiles:", tiles)

        tiles = rotateTiles(tiles)

        //print("After rotating tiles:", tiles)

        val greatTile = combineTilesToOne(tiles)

        //print( "After combining tiles to one:", listOf(greatTile))

        //val roughnessOfTheWatersBefore = greatTile.getRoughnessOfTheWaters()

        /*val foundMonster = */greatTile.findAndMarkPattern(getMonsterPattern(), 'O')

        //print("After marking monster(s), found monster: $foundMonster", listOf(greatTile))

        val roughnessOfTheWaters = greatTile.getRoughnessOfTheWaters()

        return roughnessOfTheWaters.toString()
    }

    private fun print(message: String, tiles: List<Tile>) {
        Log.d(TAG, message)
        val strings = toString(tiles)
        for (i in strings.indices) {
            Log.d(TAG, strings[i])
        }
    }

    private fun toString(tiles: List<Tile>): List<String> {
        val size = sqrt(tiles.size.toDouble()).toInt()

        val ret = mutableListOf<String>()

        var currentString = "Tiles:"
        tiles.forEachIndexed { index, tile ->
            if (index % size == 0) {
                currentString += "\n"
            }

            currentString += "${tile.id}\t"
        }

        currentString += " \n"
        ret.add(currentString)

        val tileSize = tiles[0].tileData.size

        currentString = " \n"
        var lines = 0
        for (i in 0 until size) {
            for (j in 0 until tileSize) {
                for (h in 0 until size) {
                    currentString += "${tiles[i * size + h].tileData[j]}  "
                }

                lines++
                if (lines % 24 == 0) {
                    ret.add(currentString)
                    currentString = " \n"
                } else {
                    currentString += "\n"
                }
            }
            currentString += "\n"
        }

        return ret
    }

    private fun getMonsterPattern(): List<String> {
        return listOf(
                "                  # ",
                "#    ##    ##    ###",
                " #  #  #  #  #  #   "
        )
    }

    private fun rearrangeTiles(tiles: List<Tile>): List<Tile> {

        val retTiles = mutableListOf<Tile>()

        val size = sqrt(tiles.size.toDouble()).toInt()

        // Find a corner!
        val firstCorner = tiles.find { it.isCorner }!!
        var currentTile: Tile = firstCorner

        retTiles.add(0, currentTile)
        currentTile = currentTile.neighbors.first()
        firstCorner.rightNeighbour = currentTile

        // Add all the edges to another corner in top row.
        while (currentTile.isEdge) {
            retTiles.add(currentTile)
            val foundEdge = currentTile.neighbors.find { tile ->
                tile.isEdge && retTiles.find { tile.id == it.id } == null
            }
            if (foundEdge == null) {
                currentTile = currentTile.neighbors.find { !retTiles.contains(it) && it.isCorner }!!
                retTiles.add(currentTile)
            } else {
                currentTile.rightNeighbour = foundEdge
                currentTile = foundEdge
            }
        }

        // Add all other tiles. Using the top most row as a guide.
        var currentIndex = retTiles.size
        while (retTiles.size < tiles.size) {
            val tileAbove = retTiles[currentIndex - size]
            val foundTile = tileAbove.neighbors.find { !retTiles.contains(it) }!!

            foundTile.topNeighbour = tileAbove

            if (currentIndex % size != 0) {
                foundTile.leftNeighbour = currentTile
            }

            currentTile = foundTile
            retTiles.add(currentTile)
            currentIndex = retTiles.size
        }

        return retTiles
    }

    // Given that the tiles are correctly arranged. And have the correct neighbours.
    private fun rotateTiles(tiles: List<Tile>): List<Tile> {
        val size = sqrt(tiles.size.toDouble()).toInt()

        for (i in tiles.indices) {
            tiles[i].rotateAndAlignToNeighbours(ignoreLeftAlignment = i < 1, ignoreTopAlignment = i < size, ignoreRightAlignment = true, ignoreBottomAlignment = true)
        }

        return tiles
    }

    private fun combineTilesToOne(tiles: List<Tile>): Tile {
        val size = sqrt(tiles.size.toDouble()).toInt()
        val tileSize = tiles.first().tileData.size

        val data = MutableList<String>(size * (tileSize - 2)) { "" }

        for (i in 0 until size) {
            for (j in 1 until tileSize - 1) {
                var row = ""
                for (h in 0 until size) {
                    val subTileRow = tiles[i * size + h].tileData[j]
                    row += subTileRow.substring(1, subTileRow.lastIndex)
                }
                data[(j - 1) + i * (tileSize - 2)] = row
            }
        }

        return Tile(0, data)
    }

    private fun getCornerProduct(tiles: List<Tile>): Long {
        var ret = 1L
        tiles.forEach { tile ->
            if (tile.isCorner) {
                ret *= tile.id
            }
        }
        return ret
    }

    private fun matchNeighbors(tiles: List<Tile>) {
        tiles.forEach { tile ->
            tiles.forEach { otherTile ->
                if (tile != otherTile) {
                    tile.tryToAddNeighbor(otherTile)
                }
            }
        }
    }

    private fun getTiles(context: Context, filename: String): List<Tile> {
        val ret = mutableListOf<Tile>()

        context.assets.open(filename).bufferedReader().use {
            var id = 0
            var input = mutableListOf<String>()
            it.forEachLine { line ->
                when {
                    line.startsWith("Tile ") -> {
                        val parts = line.split(' ')
                        id = parts[1].substring(0, parts[1].lastIndex).toInt()
                    }
                    line.isBlank() -> {
                        ret.add(Tile(id, input))
                        id = 0
                        input = mutableListOf()
                    }
                    else -> {
                        input.add(line)
                    }
                }
            }

            if (id != 0 && input.isNotEmpty()) {
                ret.add(Tile(id, input))
            }
        }

        return ret
    }

    companion object {
        private const val TAG: String = "Day20"
    }

    private class Tile(val id: Int, data: List<String>) {

        var tileData: List<String> = data
            private set

        val leftEdge: String
            get() = edges[0]

        val topEdge
            get() = edges[1]

        val rightEdge
            get() = edges[2]

        val bottomEdge
            get() = edges[3]

        val edges: List<String>
            get() {
                return listOf(calculateLeftEdge(),
                        tileData[0],
                        calculateRightEdge(),
                        tileData[tileData.lastIndex])
            }

        val possibleEdges: List<String> = getAllPossibleEdges()

        var neighbors: MutableSet<Tile> = mutableSetOf()
            private set

        val isCorner: Boolean
            get() {
                return neighbors.size == 2
            }

        val isEdge: Boolean
            get() {
                return neighbors.size == 3
            }

        var rightNeighbour: Tile? = null
            set(value) {

                field = value

                value?.let {
                    if (it.leftNeighbour != this) {
                        it.leftNeighbour = this
                    }
                }
            }

        var leftNeighbour: Tile? = null
            set(value) {

                field = value

                value?.let {
                    if (it.rightNeighbour != this) {
                        it.rightNeighbour = this
                    }
                }
            }

        var topNeighbour: Tile? = null
            set(value) {

                field = value

                value?.let {
                    if (it.bottomNeighbour != this) {
                        it.bottomNeighbour = this
                    }
                }
            }

        var bottomNeighbour: Tile? = null
            set(value) {

                field = value

                value?.let {
                    if (it.topNeighbour != this) {
                        it.topNeighbour = this
                    }
                }
            }

        private var usedEdges: MutableList<String> = mutableListOf()

        fun rotateAndAlignToNeighbours(ignoreLeftAlignment: Boolean, ignoreTopAlignment: Boolean, ignoreRightAlignment: Boolean, ignoreBottomAlignment: Boolean) {
            leftNeighbour?.let { neighbour ->
                val commonEdges = findMatchingEdges(neighbour)
                when {
                    isTopEdge(commonEdges) -> rotateRight()
                    isRightEdge(commonEdges) -> flipY()
                    isBottomEdge(commonEdges) -> rotateLeft()
                }

                if (!ignoreLeftAlignment) {
                    alignLeftWith(neighbour.rightEdge)
                }
            }

            topNeighbour?.let { neighbour ->
                val commonEdges = findMatchingEdges(neighbour)
                when {
                    isLeftEdge(commonEdges) -> rotateRight()
                    isRightEdge(commonEdges) -> rotateLeft()
                    isBottomEdge(commonEdges) -> flipX()
                }

                if (!ignoreTopAlignment) {
                    alignTopWith(neighbour.bottomEdge)
                }
            }

            rightNeighbour?.let { neighbour ->
                val commonEdges = findMatchingEdges(neighbour)
                when {
                    isLeftEdge(commonEdges) -> flipY()
                    isTopEdge(commonEdges) -> rotateLeft()
                    isBottomEdge(commonEdges) -> rotateRight()
                }

                if (!ignoreRightAlignment) {
                    alignRightWith(neighbour.leftEdge)
                }
            }

            bottomNeighbour?.let { neighbour ->
                val commonEdges = findMatchingEdges(neighbour)
                when {
                    isLeftEdge(commonEdges) -> rotateLeft()
                    isTopEdge(commonEdges) -> flipX()
                    isRightEdge(commonEdges) -> rotateRight()
                }

                if (!ignoreBottomAlignment) {
                    alignBottomWith(neighbour.topEdge)
                }
            }
        }

        fun getRoughnessOfTheWaters(): Int {
            var ret = 0
            tileData.forEach { row ->
                ret += row.count {
                    it == '#'
                }
            }
            return ret
        }

        fun findAndMarkPattern(pattern: List<String>, marking: Char): Int {
            for (xFlips in 0..1) {
                for (yFlips in 0..1) {
                    for (rotations in 0..3) {
//                        Log.d(TAG, "Trying to find monster in:")
//                        toStrings().forEach {
//                            Log.d(TAG, it)
//                        }

                        val numberOfFoundMonsters = findAndMarkPatternInner(pattern, marking)
                        //ret += numberOfFoundMonsters
                        if (numberOfFoundMonsters > 0) {
                            return numberOfFoundMonsters
                        }

                        rotateRight()
                    }
                    rotateRight()
                    flipY()
                }
                flipX()
            }
            return 0
        }

        private fun findAndMarkPatternInner(pattern: List<String>, marking: Char): Int {
            val tileWidth = tileData.first().length
            val size = tileWidth - pattern.first().length

            val sbData = StringBuilder()
            tileData.forEach {
                sbData.append(it + "\n")
            }
            val data = sbData.toString()

            val sb = StringBuilder()
            pattern.forEachIndexed { index, s ->
                sb.append(s.replace(' ', '.'))
                if (index < pattern.lastIndex) {
                    sb.append("(.|\\n){${size + 1}}")
                }
            }

            val regexString = sb.toString()
            val regex = regexString.toRegex()
            var numberOfMatches = 0
            val resultSb = StringBuilder(data)
            var match = regex.find(data)
            while (match != null){
                numberOfMatches++

                for (i in pattern.indices) {
                    for (j in pattern[i].indices) {
                        if (pattern[i][j] == '#') {
                            resultSb[match.range.first + j + i * (tileWidth + 1)] = marking
                        }
                    }
                }
                match = regex.find(data, match.range.first + 1)
            }

            if (numberOfMatches > 0) {
                tileData = List<String>(tileData.size) { index ->
                    resultSb.substring(index * tileWidth, (index + 1) * tileWidth)
                }
                tileData = resultSb.split('\n')
            }

            return numberOfMatches
        }

        private fun alignLeftWith(edge: String) {
            if (edge != leftEdge) {
                flipX()
            }
        }

        private fun alignTopWith(edge: String) {
            if (edge != topEdge) {
                flipY()
            }
        }

        private fun alignRightWith(edge: String) {
            if (edge != rightEdge) {
                flipX()
            }
        }

        private fun alignBottomWith(edge: String) {
            if (edge != bottomEdge) {
                flipY()
            }
        }

        private fun flipX() {
            tileData = tileData.reversed()
        }

        private fun flipY() {
            tileData = List<String>(tileData.size) { index ->
                tileData[index].reversed()
            }
        }

        fun rotateLeft() {
        val newTiles = Array<String>(tileData.size) { "" }

        tileData.forEach { line ->
            line.forEachIndexed { index, c ->
                newTiles[tileData.lastIndex - index] = c + newTiles[tileData.lastIndex - index]
            }
        }

        tileData = newTiles.toList()
        }

        fun rotateRight() {
        val newTiles = Array<String>(tileData.size) { "" }

        tileData.forEach { line ->
            line.forEachIndexed { index, c ->
                newTiles[tileData.lastIndex - index] = newTiles[tileData.lastIndex - index] + c
            }
        }

        tileData = newTiles.toList()
        }

        private fun calculateLeftEdge(): String {
            var leftEdge = ""
            tileData.forEach {
                leftEdge += it[0]
            }
            return leftEdge
        }

        private fun calculateRightEdge(): String {
            var rightEdge = ""
            tileData.forEach {
                rightEdge += it[it.lastIndex]
            }
            return rightEdge
        }

        private fun isLeftEdge(edges: List<String>) = edges.contains(leftEdge)

        private fun isTopEdge(edges: List<String>) = edges.contains(topEdge)

        private fun isRightEdge(edges: List<String>) = edges.contains(rightEdge)

        private fun isBottomEdge(edges: List<String>) = edges.contains(bottomEdge)

        fun tryToAddNeighbor(possibleNeighbor: Tile): Boolean {
            if (matchEdged(possibleNeighbor)) {
                neighbors.add(possibleNeighbor)
                possibleNeighbor.neighbors.add(this)
                return true
            }

            return false
        }

        private fun findMatchingEdges(tile: Tile): List<String> {
            return usedEdges.filter { edge -> tile.usedEdges.contains(edge) }
        }

        private fun matchEdged(otherTile: Tile): Boolean {
            possibleEdges.forEach { thisEdge ->
                val foundEdge = otherTile.possibleEdges.find { otherEdge -> thisEdge == otherEdge }
                if (foundEdge != null) {
                    if (!usedEdges.contains(thisEdge)) {
                        usedEdges.add(thisEdge)
                    }

                    if (!otherTile.usedEdges.contains(foundEdge)) {
                        otherTile.usedEdges.add(foundEdge)
                    }
                    return true
                }
            }
            return false
        }

        private fun getAllPossibleEdges(): List<String> {
            val ret = MutableList<String>(8) { "" }

            ret[0] = tileData[0]
            ret[1] = tileData[0].reversed()
            ret[2] = tileData[tileData.lastIndex]
            ret[3] = tileData[tileData.lastIndex].reversed()

            var leftEdge = ""
            var rightEdge = ""

            tileData.forEach {
                leftEdge += it[0]
                rightEdge += it[it.lastIndex]
            }

            ret[4] = leftEdge
            ret[5] = leftEdge.reversed()
            ret[6] = rightEdge
            ret[7] = rightEdge.reversed()

            return ret
        }

        fun toStrings(): List<String> {
            val ret = mutableListOf<String>()
            var currentString = "\nTile ${id.toString()}"
            tileData.forEachIndexed { index, s ->
                if (index % 25 == 0) {
                    ret.add(currentString)
                    currentString = "\n$s"
                } else {
                    currentString += "\n$s"
                }
            }
            ret.add(currentString)
            return ret
        }
    }
}