package com.example.adventofcode2020.advent

import android.content.Context

interface BaseDay {
    fun testAnswer01(context: Context): Boolean
    fun testAnswer02(context: Context): Boolean
    fun getAnswer01(context: Context): String
    fun getAnswer02(context: Context): String
}