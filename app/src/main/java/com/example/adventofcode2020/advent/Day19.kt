package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log
import java.util.*

class Day19: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val node = getRules(context, "day19_test")

        val receivedMessages = getReceivedMessages(context, "day19_test")

        val expectedResult = listOf<Boolean>(true, false, true, false, false)
        var numberOfMatchingMessages = 0

        receivedMessages.forEachIndexed { index, message ->
            val validMessage = node.validate(message)

            if (validMessage != expectedResult[index]) {
                return false
            }

            if (validMessage) {
                numberOfMatchingMessages++
            }
        }

        return numberOfMatchingMessages == 2
    }

    override fun testAnswer02(context: Context): Boolean {
        val node0 = getRules(context, "day19_test_1")

        val receivedMessages0 = getReceivedMessages(context, "day19_test_1")

        var numberOfMatchingMessages0 = 0

        receivedMessages0.forEach { message ->
            val validMessage = node0.validate(message)

            if (validMessage) {
                numberOfMatchingMessages0++
            }
        }

        if (numberOfMatchingMessages0 != 3) {
            return false
        }

        val node1 = getRules(context, "day19_test_2")

        val receivedMessages1 = getReceivedMessages(context, "day19_test_2")

        var numberOfMatchingMessages1 = 0
        val expectedResult = listOf<Boolean>(true, false, true, false, true, false, true, true, true, true, true, true, true, true, true)

        receivedMessages1.forEachIndexed { index, message ->
            val validMessage = node1.validate(message)

            if (validMessage == expectedResult[index]) {
                Log.d(TAG, "PASS: Message $message ${(if (validMessage) { "match" } else { "doesn't match" })} witch is correct.")
            } else {
                Log.d(TAG, "FAILED: Message $message ${(if (validMessage) { "match" } else { "doesn't match" })} witch is incorrect.")
            }

            if (validMessage) {
                numberOfMatchingMessages1++
            }
        }

        return numberOfMatchingMessages1 == 12
    }

    override fun getAnswer01(context: Context): String {
        val node = getRules(context, "day19")

        val receivedMessages = getReceivedMessages(context, "day19")

        var numberOfMatchingMessages = 0

        receivedMessages.forEach { message ->
            val validMessage = node.validate(message)

            if (validMessage) {
                numberOfMatchingMessages++
            }
        }

        return numberOfMatchingMessages.toString()
    }

    override fun getAnswer02(context: Context): String {
        val node = getRules(context, "day19_1")

        val receivedMessages = getReceivedMessages(context, "day19_1")

        var numberOfMatchingMessages = 0

        receivedMessages.forEach { message ->
            val validMessage = node.validate(message)

            if (validMessage) {
                numberOfMatchingMessages++
            }
        }

        return numberOfMatchingMessages.toString()
    }

    private fun getRules(context: Context, filename: String): Node {

        val rulesAsString: SortedMap<String, String> = sortedMapOf()
        context.assets.open(filename).bufferedReader().use {
            var line = it.readLine()
            while (line.isNotBlank()) {
                val parts = line.split(':')

                val id = parts[0]
                val rule = parts[1].trim()
                rulesAsString[id] = rule
                line = it.readLine()
            }
        }

        return createNode(rulesAsString)
    }

    private fun getReceivedMessages(context: Context, filename: String): List<String> {
        var foundFirstMessage = false
        val ret = mutableListOf<String>()
        context.assets.open(filename).bufferedReader().use {
            it.forEachLine { line ->
                if (foundFirstMessage) {
                    if (!line.startsWith("//")) {
                        ret.add(line)
                    }
                }

                if (line.isBlank()) {
                    foundFirstMessage = true
                }
            }
        }

        return ret
    }

    private fun createNode(rules: SortedMap<String, String>): Node {
        val nodeList: SortedMap<String, Node> = sortedMapOf()

        return createNodeInner(rules, rules.keys.first(), nodeList)
    }

    private fun createNodeInner(rules: SortedMap<String, String>,
                                currentId: String,
                                nodeCache: SortedMap<String, Node>): Node {

        if (nodeCache.containsKey(currentId)) {
            val ret = nodeCache[currentId]
            if (ret != null) {
                return ret
            }
        }

        val rule = rules[currentId]!!

        return when {
            // This is an or node, which can also contains two and nodes.
            rule.contains('|') -> {
                createOrNode(rules, currentId, rule, nodeCache)
            }
            rule.contains('"') -> { // This is a leaf
                createLeaf(currentId, rule.replace("\"", ""), nodeCache)
            }
            else -> { // This is an and node.
                createAndNode(rules, currentId, rule, nodeCache)
            }
        }
    }

    private fun createOrNode(rules: SortedMap<String, String>,
                             id: String,
                             rule: String,
                            nodeCache: SortedMap<String, Node>): Node {
        val orNode = Or(id)
        nodeCache[id] = orNode

        val parts = rule.split(" | ")

        if (isAndRule(parts[0])) {
            val childNode = createAndNode(rules,"${id}a", parts[0], nodeCache)
            orNode.addChild(childNode)
        } else {
            val childNode = createNodeInner(rules, parts[0].trim(), nodeCache)
            orNode.addChild(childNode)
        }

        if (isAndRule(parts[1])) {
            val childNode = createAndNode(rules,"${id}b", parts[1], nodeCache)
            orNode.addChild(childNode)
        } else {
            val childNode = createNodeInner(rules, parts[0].trim(), nodeCache)
            orNode.addChild(childNode)
        }
        return orNode
    }

    private fun createAndNode(rules: SortedMap<String, String>,
                              id: String,
                              rule: String,
                              nodeCache: SortedMap<String, Node>): Node {
        val andNode = And(id)
        nodeCache[id] = andNode

        val parts = rule.split(" ")

        parts.forEach { childId ->
            val child = createNodeInner(rules, childId, nodeCache)
            andNode.addChild(child)
        }

        return andNode
    }

    private fun createLeaf(id: String, rule: String, nodeCache: SortedMap<String, Node>): Node {
        val ret = Leaf(id, rule.first())
        nodeCache[id] = ret
        return ret
    }

    private fun isAndRule(rule: String): Boolean {
        return rule.matches("([0-9]( ?))+\\w+".toRegex())
    }

    private abstract class Node(val id: String) {

        protected val children: MutableList<Node> = mutableListOf()

        fun validate(data: String): Boolean {
            val result = validateInner(data)

            if (result.isEmpty()) {
                return false
            }

            return result.find { it == data.length } != null
        }

        abstract fun validateInner(data: String, indexToCheck: Int = 0): List<Int>

        fun addChild(childNode: Node) {
            children.add(childNode)
        }
    }

    private class And(id: String): Node(id) {

        override fun validateInner(data: String, indexToCheck: Int): List<Int> {

            if (indexToCheck > data.lastIndex) {
                return listOf()
            }

            var result: List<Int> = listOf()
            for (node in children) {
                result = if (result.isEmpty()) {
                    node.validateInner(data, indexToCheck)
                } else {
                    val ret = mutableListOf<Int>()
                    result.forEach { currentIndex ->
                        if (currentIndex <= data.lastIndex) {
                            ret += node.validateInner(data, currentIndex)
                        }
                    }
                    ret
                }

                if (result.isEmpty()) {
                    return listOf()
                }
            }

            return result
        }
    }

    private class Or(id: String): Node(id) {

        override fun validateInner(data: String, indexToCheck: Int): List<Int> {
            //Log.d(TAG, "Validating OR, $id, current index $indexToCheck ${data.substring(indexToCheck)}, data $data")

            if (indexToCheck > data.lastIndex) {
                return listOf()
            }

            val ret = mutableListOf<Int>()

            if (indexToCheck < data.length) {
                for (i in  children.indices) {
                    val childResponse = children[i].validateInner(data, indexToCheck)
                    if (childResponse.isNotEmpty()) {
                        ret += childResponse
                    }
                }
            }

            if (ret.isNotEmpty()) {
                return ret
            }
            return listOf()
        }

    }

    private class Leaf(id: String, val token: Char): Node(id) {
        override fun validateInner(data: String, indexToCheck: Int): List<Int> {

            if (indexToCheck > data.lastIndex) {
                return listOf()
            }

            return if (data[indexToCheck] == token) {
                listOf(indexToCheck + 1)
            } else {
                listOf()
            }
        }
    }

    companion object {
        private const val TAG: String = "Day19"
        private const val INVALID_INDEX: Int = -1
    }
}