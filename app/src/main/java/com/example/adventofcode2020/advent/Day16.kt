package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log

class Day16: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val ticketData = getInput(context, "day16_test", false)

        val result = ticketData.calculateScanningError()
        return result == 71
    }

    override fun testAnswer02(context: Context): Boolean {
        val ticketData = getInput(context, "day16_test_1", true)

        val orderedRules = ticketData.getOrderedRules()

        return orderedRules[0].name == "row" &&
                orderedRules[1].name == "class" &&
                orderedRules[2].name == "seat"
    }

    override fun getAnswer01(context: Context): String {
        val ticketData = getInput(context, "day16", false)

        val result = ticketData.calculateScanningError()
        return result.toString()
    }

    override fun getAnswer02(context: Context): String {
        val ticketData = getInput(context, "day16", true)

        val orderedRules = ticketData.getOrderedRules()

        val filteredValues = ticketData.getYourTicketValuesWithTitle(orderedRules)

        var product: Long = 1
        filteredValues.forEach {
            if (it.key.contains("departure")) {
                product *= it.value
            }
        }

        return product.toString()
    }

    private fun getInput(context: Context, filename: String, ignoreInvalid: Boolean): TicketData {

        val rules = mutableListOf<String>()
        val yourTicket = mutableListOf<String>()
        val nearbyTickets = mutableListOf<String>()

        var currentList = rules

        context.assets.open(filename).bufferedReader().use {
            it.forEachLine { line ->
                if (line.isNotBlank()) {
                    when (line) {
                        "your ticket:" -> {
                            currentList = yourTicket
                        }
                        "nearby tickets:" -> {
                            currentList = nearbyTickets
                        }
                        else -> {
                            currentList.add(line)
                        }
                    }
                }
            }

            return TicketData(rules, yourTicket, nearbyTickets, ignoreInvalid)
        }
    }

    private class TicketData(rules: List<String>, yourTicket: List<String>, nearbyTicket: List<String>, ignoreInvalid: Boolean) {

        private val rules: List<Rule> = createRules(rules)
        private val yourTicket: IntArray = createYourTicket(yourTicket)
        private val nearbyTickets: List<IntArray> = createNearbyTickets(nearbyTicket, ignoreInvalid)

        private val orderedRules = mutableListOf<Rule>()

        fun calculateScanningError(): Int {
            val invalidNumbers = mutableListOf<Int>()

            nearbyTickets.forEach { ticket ->
                ticket.forEach { num ->
                    if (!checkAnyRule(num)) {
                        invalidNumbers.add(num)
                    }
                }
            }

            return invalidNumbers.sum()
        }

        private fun checkAnyRule(num: Int): Boolean {

            rules.forEach { rule ->
                if (rule.fulfilled(num)) {
                    return true
                }
            }
            return false
        }

        private fun createRules(rules: List<String>): List<Rule> {
            val ret = mutableListOf<Rule>()
            rules.forEach {
                ret.add(Rule(it))
            }
            return ret
        }

        private fun createYourTicket(input: List<String>): IntArray {
            return input.first().split(',').map { it -> it.toInt() }.toIntArray()
        }

        private fun createNearbyTickets(input: List<String>, ignoreInvalid: Boolean = false): List<IntArray> {
            val ret = mutableListOf<IntArray>()

            input.forEach { ticketString ->
                val ticket = ticketString.split(',').map { it -> it.toInt() }.toIntArray()
                if (!ignoreInvalid || isTicketValid(ticket)) {
                    ret.add(ticket)
                }
                // For debug.
//                else {
//                    Log.d(TAG, "Ignored ${ticketString}")
//                }
            }

            return ret
        }

        private fun isTicketValid(ticket: IntArray): Boolean {
            ticket.forEach { num ->
                if (!checkAnyRule(num)) {
                    return false
                }
            }
            return true
        }

        fun getOrderedRules(): Array<Rule> {

            val orderedRules = Array<Rule?>(rules.size) { null }
            val unorderedRules: MutableList<Rule> = MutableList<Rule>(rules.size) { it -> rules[it] }
            val tickets: List<IntArray> = mutableListOf(yourTicket) + nearbyTickets
            val indicesToIgnore = mutableListOf<Int>()
            while (unorderedRules.isNotEmpty()) {
                for (i in yourTicket.indices) {
                    if (!indicesToIgnore.contains(i)) {
                        val rule = getRuleThatFulfillIndex(tickets, unorderedRules, i)
                        if (rule != null) {
                            orderedRules[i] = rule
                            unorderedRules.remove(rule)
                            indicesToIgnore.add(i)
                        }
                    }
                }
            }

            return orderedRules.requireNoNulls()
        }

        private fun getRuleThatFulfillIndex(tickets: List<IntArray>, rulesToCheck: List<Rule>, index: Int): Rule? {

            val rulesToTest: MutableList<Rule> = MutableList(rulesToCheck.size) { it -> rulesToCheck[it] }
            tickets.forEach { ticket ->
                getNonFulfilledRules(ticket[index], rulesToCheck).forEach {
                    rulesToTest.remove(it)
                }
            }

            if (rulesToTest.size == 1) {
                return rulesToTest.first()
            }
            return null
        }

        private fun getNonFulfilledRules(num: Int, rulesToCheck: List<Rule>): List<Rule> {
            val ret = mutableListOf<Rule>()
            rulesToCheck.forEach { rule ->
                if (!rule.fulfilled(num)) {
                    ret.add(rule)
                }
            }
            return ret
        }

        fun getYourTicketValuesWithTitle(rules: Array<Rule>): Map<String, Int> {
            val ret = mutableMapOf<String, Int>()

            rules.forEachIndexed { index, rule ->
                ret[rule.name] = yourTicket[index]
            }

            return ret
        }

        companion object {
            private const val TAG: String = "Day16"
        }

        class Rule(data: String) {

            val name: String
            private val intervals: IntArray

            init {
                val colonIndex = data.indexOfFirst { it == ':' }
                name = data.substring(0, colonIndex)

                val intervalsS = data.substring(colonIndex + 1, data.length)
                val intervalsList = mutableListOf<Int>()
                intervalsS.split("or").forEach {
                    val interval = it.split('-')
                    intervalsList.add(interval[0].trim().toInt())
                    intervalsList.add(interval[1].trim().toInt())
                }
                intervals = intervalsList.toIntArray()
            }

            fun fulfilled(num: Int): Boolean {
                for (i in intervals.indices step 2) {
                    if (num >= intervals[i] && num <= intervals[i + 1]) {
                        return true
                    }
                }
                return false
            }
        }
    }
}