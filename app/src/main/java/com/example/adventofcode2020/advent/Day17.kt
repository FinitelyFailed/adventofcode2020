package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log

class Day17: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val space = getInput3d(context, "day17_test")

        space.run(6)
        val result = space.getNumOfActiveCubes()
        return result == 112
    }

    override fun testAnswer02(context: Context): Boolean {
        val space = getInput4d(context, "day17_test")

        space.run(6)
        val result = space.getNumOfActiveCubes()
        return result == 848
    }

    override fun getAnswer01(context: Context): String {
        val space3d = getInput3d(context, "day17")

        space3d.run(6)
        val result = space3d.getNumOfActiveCubes()
        return result.toString()
    }

    override fun getAnswer02(context: Context): String {
        val space3d = getInput4d(context, "day17")

        space3d.run(6)
        val result = space3d.getNumOfActiveCubes()
        return result.toString()
    }

    private fun getInput3d(context: Context, filename: String): Space3d {
        context.assets.open(filename).bufferedReader().use {

            val space = Space3d()

            var x = space.size / 2
            var y = space.size / 2
            val z = space.size / 2

            it.forEachLine { row ->
                row.forEach { area ->
                    if (area == '#') {
                        space.activateCube(x, y, z)
                    }
                   x++
                }
                x = space.size / 2
                y++
            }
            return space
        }
    }

    private fun getInput4d(context: Context, filename: String): Space4d {
        context.assets.open(filename).bufferedReader().use {

            val space = Space4d()

            var x = space.size / 2
            var y = space.size / 2
            val z = space.size / 2
            val w = space.size / 2

            it.forEachLine { row ->
                row.forEach { area ->
                    if (area == '#') {
                        space.activateCube(x, y, z, w)
                    }
                    x++
                }
                x = space.size / 2
                y++
            }

            return space
        }
    }

    private class Space3d() {

        val size = 100

        private val space: Array<Array<Array<Cube3d>>> =
                Array(size) { x ->
                    Array(size) { y ->
                        Array(size) { z ->
                            Cube3d(x, y, z)
                        }
                    }
                }

        private val cubes = mutableListOf<Cube3d>()

        fun run(turns: Int) {
            //Log.v(TAG, "Init state:")
            //print()
            for (i in 0 until turns) {
                runTurn()

                //Log.v(TAG, "Turn: $i")
                //print()
            }
        }

        fun getNumOfActiveCubes(): Int {
            return cubes.size
        }

        private fun runTurn() {
            val cubesToRemove = mutableListOf<Cube3d>()
            val cubesToAdd = mutableListOf<Cube3d>()

            var numberOfNeighbors = 0

            cubes.forEach { c ->
                for (z in (c.z - 1)..(c.z + 1)) {
                    for (y in (c.y - 1)..(c.y + 1)) {
                        for (x in (c.x - 1)..(c.x + 1)) {
                            if (x != c.x || y != c.y || z != c.z) {
                                if (isOccupied(x, y, z)) {
                                    numberOfNeighbors++
                                } else {
                                    if (getNumberOfNeighbors(x, y, z) == 3) {
                                        val nC = space[x][y][z]
                                        if (!cubesToAdd.contains(nC)) {
                                            cubesToAdd.add(nC)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (numberOfNeighbors != 2 &&
                        numberOfNeighbors != 3 &&
                        !cubesToRemove.contains(c)) {
                    cubesToRemove.add(c)
                    //Log.v(TAG, "Cube ${c.x} ${c.y} ${c.z} with $numberOfNeighbors")
                }

                numberOfNeighbors = 0
            }

            cubesToAdd.forEach { c ->
                activateCube(c)
            }

            cubesToRemove.forEach { c ->
                deactivateCube(c)
            }
        }

        fun getNumberOfNeighbors(cX: Int, cY: Int, cZ: Int): Int {
            var ret = 0
            for (x in (cX - 1)..(cX + 1)) {
                for (y in (cY - 1)..(cY + 1)) {
                    for (z in (cZ - 1)..(cZ + 1)) {
                        if (x != 0 && y != 0 && z != 0) {
                            if (isOccupied(x, y, z)) {
                                ret++
                            }
                        }
                    }
                }
            }
            return ret
        }

        fun activateCube(x: Int, y: Int, z: Int) {
            space[x][y][z].active = true
            cubes.add(space[x][y][z])

            //Log.v(TAG, "activateCube $x, $y, $z")
        }

        private fun activateCube(cube: Cube3d) {
            activateCube(cube.x, cube.y, cube.z)
        }

        fun deactivateCube(x: Int, y: Int, z: Int) {
            space[x][y][z].active = false
            cubes.remove(space[x][y][z])

            //Log.v(TAG, "deactivateCube $x, $y, $z")
        }

        private fun deactivateCube(cube: Cube3d) {
            deactivateCube(cube.x, cube.y, cube.z)
        }

        fun isOccupied(x: Int, y: Int, z: Int): Boolean {
            return space[x][y][z].active
        }

        private fun print() {

            cubes.sortBy { c -> c.z }

            var xMin = Int.MAX_VALUE
            var xMax = Int.MIN_VALUE
            var yMin = Int.MAX_VALUE
            var yMax = Int.MIN_VALUE
            var zMin = Int.MAX_VALUE
            var zMax = Int.MIN_VALUE

            cubes.forEach { c ->
                if (xMin > c.x) xMin = c.x
                if (xMax < c.x) xMax = c.x

                if (yMin > c.y) yMin = c.y
                if (yMax < c.y) yMax = c.y

                if (zMin > c.z) zMin = c.z
                if (zMax < c.z) zMax = c.z
            }

            var level = ""
            for (z in zMin..zMax) {
                for (y in yMin..yMax) {
                    for (x in xMin..xMax) {
                        level += if (isOccupied(x, y, z)) {
                            "#"
                        } else {
                            "."
                        }
                    }
                    level += "\n"
                }
                Log.v(TAG, "z:${z - size / 2} \n$level")
                level = ""
            }
        }

        companion object {
            private const val TAG: String = "Day17 3d"
        }
    }

    private class Cube3d(val x: Int, val y: Int, val z: Int, var active: Boolean = false)


    private class Space4d() {

        val size = 25

        private val space: Array<Array<Array<Array<Cube4d>>>> =
                Array(size) { x ->
                    Array(size) { y ->
                        Array(size) { z ->
                            Array(size) { w ->
                                Cube4d(x, y, z, w)
                            }
                        }
                    }
                }

        private val cubes = mutableListOf<Cube4d>()

        fun run(turns: Int) {
            //Log.v(TAG, "Init state:")
            //print()
            for (i in 0 until turns) {
                runTurn()

                //Log.v(TAG, "Turn: $i")
                //print()
            }
        }

        fun getNumOfActiveCubes(): Int {
            return cubes.size
        }

        private fun runTurn() {
            val cubesToRemove = mutableListOf<Cube4d>()
            val cubesToAdd = mutableListOf<Cube4d>()

            var numberOfNeighbors = 0

            cubes.forEach { c ->
                for (w in (c.w - 1)..(c.w + 1)) {
                    for (z in (c.z - 1)..(c.z + 1)) {
                        for (y in (c.y - 1)..(c.y + 1)) {
                            for (x in (c.x - 1)..(c.x + 1)) {
                                if (x != c.x || y != c.y || z != c.z || w != c.w) {
                                    if (isOccupied(x, y, z, w)) {
                                        numberOfNeighbors++
                                    } else {
                                        if (getNumberOfNeighbors(x, y, z, w) == 3) {
                                            val nC = space[x][y][z][w]
                                            if (!cubesToAdd.contains(nC)) {
                                                cubesToAdd.add(nC)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (numberOfNeighbors != 2 &&
                        numberOfNeighbors != 3 &&
                        !cubesToRemove.contains(c)) {
                    cubesToRemove.add(c)
                    //Log.v(TAG, "Cube ${c.x} ${c.y} ${c.z} with $numberOfNeighbors")
                }

                numberOfNeighbors = 0
            }

            cubesToAdd.forEach { c ->
                activateCube(c)
            }

            cubesToRemove.forEach { c ->
                deactivateCube(c)
            }
        }

        fun getNumberOfNeighbors(cX: Int, cY: Int, cZ: Int, cW: Int): Int {
            var ret = 0
            for (x in (cX - 1)..(cX + 1)) {
                for (y in (cY - 1)..(cY + 1)) {
                    for (z in (cZ - 1)..(cZ + 1)) {
                        for (w in (cW - 1)..(cW + 1)) {
                            if (x != 0 && y != 0 && z != 0 && w != 0) {
                                if (isOccupied(x, y, z, w)) {
                                    ret++
                                }
                            }
                        }
                    }
                }
            }
            return ret
        }

        fun activateCube(x: Int, y: Int, z: Int, w: Int) {
            space[x][y][z][w].active = true
            cubes.add(space[x][y][z][w])

            //Log.v(TAG, "activateCube $x, $y, $z")
        }

        private fun activateCube(cube: Cube4d) {
            activateCube(cube.x, cube.y, cube.z, cube.w)
        }

        fun deactivateCube(x: Int, y: Int, z: Int, w: Int) {
            space[x][y][z][w].active = false
            cubes.remove(space[x][y][z][w])

            //Log.v(TAG, "deactivateCube $x, $y, $z")
        }

        private fun deactivateCube(cube: Cube4d) {
            deactivateCube(cube.x, cube.y, cube.z, cube.w)
        }

        fun isOccupied(x: Int, y: Int, z: Int, w: Int): Boolean {
            return space[x][y][z][w].active
        }

        private fun print() {

            cubes.sortBy { c -> c.z + c.w }

            var xMin = Int.MAX_VALUE
            var xMax = Int.MIN_VALUE
            var yMin = Int.MAX_VALUE
            var yMax = Int.MIN_VALUE
            var zMin = Int.MAX_VALUE
            var zMax = Int.MIN_VALUE
            var wMin = Int.MAX_VALUE
            var wMax = Int.MIN_VALUE

            cubes.forEach { c ->
                if (xMin > c.x) xMin = c.x
                if (xMax < c.x) xMax = c.x

                if (yMin > c.y) yMin = c.y
                if (yMax < c.y) yMax = c.y

                if (zMin > c.z) zMin = c.z
                if (zMax < c.z) zMax = c.z

                if (wMin > c.w) wMin = c.w
                if (wMax < c.w) wMax = c.w
            }

            var level = ""
            for (w in wMin..wMax) {
                for (z in zMin..zMax) {
                    for (y in yMin..yMax) {
                        for (x in xMin..xMax) {
                            level += if (isOccupied(x, y, z, w)) {
                                "#"
                            } else {
                                "."
                            }
                        }
                        level += "\n"
                    }
                    Log.v(TAG, "z:${z - size / 2} w:${w - size / 2} \n$level")
                    level = ""
                }
            }
        }

        companion object {
            private const val TAG: String = "Day17 4D"
        }
    }

    private class Cube4d(val x: Int, val y: Int, val z: Int, val w: Int, var active: Boolean = false)
}