package com.example.adventofcode2020.advent

import android.content.Context

class Day13: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val filename = "day13_test"
        val earliestTime = getEarliestTime(context, filename)
        val buses = getBussSchedules(context, filename)

        val solution = solve1(earliestTime, buses)
        return solution == 295L
    }

    override fun testAnswer02(context: Context): Boolean {
        var filename = "day13_test"
        var buses = getBussSchedules(context, filename)

        var solution = solve2(buses)
        if (solution != 1068781L) {
            return false
        }

        filename = "day13_test_1"
        buses = getBussSchedules(context, filename)

        solution = solve2(buses)
        if (solution != 754018L) {
            return false
        }

        filename = "day13_test_2"
        buses = getBussSchedules(context, filename)

        solution = solve2(buses)
        if (solution != 779210L) {
            return false
        }

        filename = "day13_test_3"
        buses = getBussSchedules(context, filename)

        solution = solve2(buses)
        if (solution != 1261476L) {
            return false
        }

        filename = "day13_test_4"
        buses = getBussSchedules(context, filename)

        solution = solve2(buses)
        if (solution != 1202161486L) {
            return false
        }

        filename = "day13_test_5"
        buses = getBussSchedules(context, filename)

        solution = solve2(buses)
        if (solution != 3417L) {
            return false
        }

        return true
    }

    override fun getAnswer01(context: Context): String {
        val filename = "day13"
        val earliestTime = getEarliestTime(context, filename)
        val buses = getBussSchedules(context, filename)

        val solution = solve1(earliestTime, buses)
        return solution.toString()
    }

    override fun getAnswer02(context: Context): String {
        val filename = "day13"
        val buses = getBussSchedules(context, filename)

        val solution = solve2(buses)
        return solution.toString()
    }

    private fun solve1(earliestDepartureTime: Long, buses: Array<Long?>): Long {
        val departureTimes = getNearestDepartures(earliestDepartureTime, buses)

        var minValIndex: Int = -1
        var minValue = Long.MAX_VALUE
        for (i in departureTimes.indices) {
            if (departureTimes[i] < minValue) {
                minValue = departureTimes[i]
                minValIndex = i
            }
        }

        if (minValIndex != -1) {
            val waitTime = departureTimes[minValIndex] - earliestDepartureTime
            val bus = buses[minValIndex]
            return if (bus == null) {
                -1
            } else {
                waitTime * bus
            }
        }

        return -1
    }

    private fun getNearestDepartures(earliestDepartureTime: Long, buses: Array<Long?>): LongArray {
        val departureTimes = LongArray(buses.size)
        for (i in buses.indices) {
            val bus = buses[i]
            if (bus == null) {
                departureTimes[i] = Long.MAX_VALUE
            } else {
                val factor = (earliestDepartureTime / bus) + 1
                departureTimes[i] = bus * factor
            }
        }
        return departureTimes
    }

    // TODO: Redo, this is cheating.
    private fun solve2(buses: Array<Long?>): Long {

        val busesWithOutNull = buses.filterNotNull()
        val remainders = IntArray(busesWithOutNull.size)
        var index = 0
        for (i in buses.indices) {
            if (buses[i] != null) {
                remainders[index] = -i
                index++
            }
        }

        var product = 1L
        for (i in busesWithOutNull.indices) {
            product *= busesWithOutNull[i]
        }

        val partialProducts = LongArray(busesWithOutNull.size) { i ->
            product / busesWithOutNull[i]
        }

        val inverse = LongArray(busesWithOutNull.size) { i ->
            computeInverse(partialProducts[i], busesWithOutNull[i])
        }

        var sum = 0L
        for (i in busesWithOutNull.indices) {
            sum += partialProducts[i] * inverse[i] * remainders[i]
        }

        return sum % product
    }

    private fun computeInverse(aVal: Long, bVal: Long): Long {
        var a = aVal
        var b = bVal
        //val m = b
        var t: Long
        var q: Long
        var x = 0L
        var y = 1L

        if (b == 1L) {
            return 0
        }

        // Apply extended Euclid Algorithm
        while (a > 1) {
            // q is quotient
            q = a / b
            t = b

            // m is remainder now, process
            // same as euclid's algo
            b = a % b
            a = t
            t = x
            x = y - q * x
            y = t
        }

        // Make x1 positive
//        if (y < 0) {
//            y += m
//        }

        return y
    }

    private fun getEarliestTime(context: Context, filename: String): Long {
        context.assets.open(filename).bufferedReader().use {
            val s = it.readLine()
            return s.toLong()
        }
    }

    private fun getBussSchedules(context: Context, filename: String): Array<Long?> {
        context.assets.open(filename).bufferedReader().use {
            it.readLine()
            val s = it.readLine()

            val ret: MutableList<Long?> = mutableListOf<Long?>()
            s.split(",").forEach { buss ->
                ret.add(buss.toLongOrNull())
            }

            return ret.toTypedArray()
        }
    }
}