package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log
import java.util.*

class Day18: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val expressions = getInput(context, "day18_test")

        var success = true
        expressions.forEach { exp ->
            val isCorrect = exp.test()
//            if (isCorrect) {
//                Log.d(TAG, "Test ans 1 Correct: ${exp.toString()}")
//            } else {
//                Log.d(TAG, "Test ans 1 Incorrect: ${exp.toString()}")
//            }
            success = isCorrect and success
        }

        return success
    }

    override fun testAnswer02(context: Context): Boolean {
        val expressions = getInput(context, "day18_test_2")

        var success = true
        expressions.forEach { exp ->
            val isCorrect = exp.test2()
//            if (isCorrect) {
//                Log.d(TAG, "Test ans 2 Correct: ${exp.toString()}")
//            } else {
//                Log.d(TAG, "Test ans 2 Incorrect: ${exp.toString()}")
//            }
            success = isCorrect and success
        }

        return success
    }

    override fun getAnswer01(context: Context): String {
        val expressions = getInput(context, "day18")

        var sum: Long = 0
        expressions.forEach { exp ->
            val solution = exp.solve()
            sum += solution
            //Log.d(TAG, "Ans 1 ${exp.toString()} = $solution")
        }

        return sum.toString()
    }

    override fun getAnswer02(context: Context): String {
        val expressions = getInput(context, "day18")

        var sum: Long = 0
        expressions.forEach { exp ->
            val solution = exp.solve2()
            sum += solution
            //Log.d(TAG, "Ans 2 ${exp.toString()} = $solution")
        }

        return sum.toString()
    }

    private fun getInput(context: Context, filename: String): List<Expression> {
        val ret = mutableListOf<Expression>()
        context.assets.open(filename).bufferedReader().use {
            it.forEachLine { line ->
                ret.add(Expression(line))
            }
        }
        return ret
    }

    companion object {
        private const val TAG: String = "Day18"
    }

    private class Expression(val data: String) {

        private var expectedSolution: Long? = null
        private val rrpnTokens: String
        private val rrpnDiffPrecedenceTokens: String

        init {
            var d = data.replace("\\s".toRegex(), "")

            if (d.contains('=')) {
                val parts = d.split('=')
                d = parts[0]
                expectedSolution = parts[1].toLong()
            }

            rrpnTokens = toReversedReversedPolishNotation(d)
            rrpnDiffPrecedenceTokens = toReversedReversedPolishNotationDiffPrecedence(d)
        }

        fun test(): Boolean {
            val solution = solveRPN(rrpnTokens)
            expectedSolution?.let {
                return it == solution
            }
            return false
        }

        fun solve(): Long {
            return solveRPN(rrpnTokens)
        }

        fun test2(): Boolean {
            val solution = solveRPN(rrpnDiffPrecedenceTokens)
            expectedSolution?.let {
                return it == solution
            }
            return false
        }

        fun solve2(): Long {
            return solveRPN(rrpnDiffPrecedenceTokens)
        }

        override fun toString(): String {
            return data
        }

        private fun toReversedReversedPolishNotation(data: String): String {
            val outputStack: Stack<Char> = Stack()
            val operatorStack: Stack<Char> = Stack()

            data.reversed().forEach { c ->
                when {
                    Character.isDigit(c) -> {
                        outputStack.push(c)
                    }
                    c == '(' -> {
                        while (operatorStack.peek() != ')') {
                            outputStack.push(operatorStack.pop())
                        }

                        if (operatorStack.peek() == ')') {
                            operatorStack.pop()
                        }
                    }
                    else -> {
                        operatorStack.push(c)
                    }
                }
            }

            while (operatorStack.isNotEmpty()) {
                outputStack.push(operatorStack.pop())
            }

            var ret = ""
            outputStack.forEach {
                ret += it
            }
            return ret
        }

        private fun toReversedReversedPolishNotationDiffPrecedence(data: String): String {
            val outputStack: Stack<Char> = Stack()
            val operatorStack: Stack<Char> = Stack()

            data.reversed().forEach { c ->
                when {
                    Character.isDigit(c) -> {
                        outputStack.push(c)
                    }
                    c == '(' -> {
                        while (operatorStack.peek() != ')') {
                            outputStack.push(operatorStack.pop())
                        }

                        if (operatorStack.peek() == ')') {
                            operatorStack.pop()
                        }
                    }
                    else -> {
                        while (operatorStack.isNotEmpty() && hasHigherPrecedence(operatorStack.peek(), c)) {
                            outputStack.push(operatorStack.pop())
                        }
                        operatorStack.push(c)
                    }
                }
            }

            while (operatorStack.isNotEmpty()) {
                outputStack.push(operatorStack.pop())
            }

            var ret = ""
            outputStack.forEach {
                ret += it
            }
            return ret
        }

        private fun hasHigherPrecedence(op0: Char, op1: Char): Boolean {
            return (op1 != ')') &&
                    ((op0 == '+' && op1 == '*') ||
                     (op0 == '+' && op1 == '/') ||
                     (op0 == '+' && op1 == '-') ||

                     (op0 == '-' && op1 == '*') ||
                     (op0 == '-' && op1 == '/') ||

                     (op0 == '*' && op1 == '/') ||
                     (op0 == op1))
        }

        private fun solveRPN(tokens: String): Long {
            val numbers: Stack<Long> = Stack()

            tokens.forEach { token ->
                if (isOperator(token)) {
                    val a: Long = numbers.pop()
                    val b: Long = numbers.pop()

                    when (token) {
                        '+' -> numbers.push(a + b)
                        '-' -> numbers.push(a - b)
                        '*' -> numbers.push(a * b)
                        '/' -> numbers.push(a / b)
                    }
                } else {
                    val n = Character.getNumericValue(token).toLong()
                    numbers.push(n)
                }
            }

            return numbers.firstElement()
        }

        private fun isOperator(token: Char): Boolean {
            return token == '+' || token == '-' || token == '*' || token == '/'
        }
    }
}