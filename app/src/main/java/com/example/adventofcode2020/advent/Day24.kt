package com.example.adventofcode2020.advent

import android.content.Context

class Day24: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val input = getInput(context, "day24_test")

        val centralTile = flipTiles(input)

        val numOfBlackTiles = centralTile.countBlackTiles()

        return numOfBlackTiles == 10
    }

    override fun testAnswer02(context: Context): Boolean {
        val input = getInput(context, "day24_test")

        val centralTile = flipTiles(input)

        val numOfBlackTiles = centralTile.runExhibit(100)

        return numOfBlackTiles == 2208
    }

    override fun getAnswer01(context: Context): String {
        val input = getInput(context, "day24")

        val centralTile = flipTiles(input)

        val numOfBlackTiles = centralTile.countBlackTiles()

        return numOfBlackTiles.toString()
    }

    override fun getAnswer02(context: Context): String {
        val input = getInput(context, "day24")

        val centralTile = flipTiles(input)

        val numOfBlackTiles = centralTile.runExhibit(100)

        return numOfBlackTiles.toString()
    }

    private fun getInput(context: Context, filename: String): List<String> {
        context.assets.open(filename).bufferedReader().use {
            return it.readLines()
        }
    }

    private fun flipTiles(instructions: List<String>): CentralTile {
        val centralTile = CentralTile(Pos(0, 0, 0))

        centralTile.flip(instructions)

        return centralTile
    }

    private class CentralTile(pos: Pos): Tile(pos) {

        private val cache = mutableMapOf<String, Tile>()

        init {
            cache[pos.toString()] = this
        }

        fun flip(instructions: List<String>): Int {
            instructions.forEach { instruction ->
                flipInternal(instruction, 0, cache)
            }

            var numberOfBlackTiles = 0
            cache.forEach {
                if (it.value.isBlack) {
                    numberOfBlackTiles++
                }
            }

            return numberOfBlackTiles
        }

        fun runExhibit(turns: Int): Int {

            var numOfBlackTiles = countBlackTiles()
            for (i in 0 until turns) {
                resetVisited()
                val listOfTilesToFlip = mutableListOf<Tile>()
                val blackTiles = getBlackTiles()
                blackTiles.forEach { blackTile ->
                    listOfTilesToFlip += blackTile.runExhibit(cache)
                }
                //resetVisited()
                // val listOfTilesToFlip = runExhibitInner(cache)

                listOfTilesToFlip.forEach {
                    it.flip()
                }

                resetVisited()
                numOfBlackTiles = countBlackTiles()
            }

            return numOfBlackTiles
        }

        private fun getBlackTiles(): List<Tile> {
            return cache.values.filter { it.isBlack }
        }

        private fun resetVisited() {
            cache.forEach {
                it.value.visited = false
            }
        }
    }

    private open class Tile(val pos: Pos) {

        var isBlack = false

        private fun getWestTile(cache: MutableMap<String, Tile>): Tile {
            if (wT == null) {
                val p = Pos(pos.x - 1, pos.y + 1, pos.z)
                val k = p.toString()
                wT = if (cache.containsKey(k)) {
                    cache[k]
                } else {
                    val t = Tile(p)
                    cache[k] = t
                    t
                }
                wT!!.eT = this
            }
            return wT!!
        }

        private fun getEastTile(cache: MutableMap<String, Tile>): Tile {
            if (eT == null) {
                val p = Pos(pos.x + 1, pos.y - 1, pos.z)
                val k = p.toString()
                eT = if (cache.containsKey(k)) {
                    cache[k]
                } else {
                    val t = Tile(p)
                    cache[k] = t
                    t
                }
                eT!!.wT = this
            }
            return eT!!
        }

        private fun getSouthWestTile(cache: MutableMap<String, Tile>): Tile {
            if (swT == null) {
                val p = Pos(pos.x - 1, pos.y, pos.z + 1)
                val k = p.toString()
                swT = if (cache.containsKey(k)) {
                    cache[k]
                } else {
                    val t = Tile(p)
                    cache[k] = t
                    t
                }
                swT!!.neT = this
            }
            return swT!!
        }

        private fun getNorthWestTile(cache: MutableMap<String, Tile>): Tile {
            if (nwT == null) {
                val p = Pos(pos.x, pos.y + 1, pos.z - 1)
                val k = p.toString()
                nwT = if (cache.containsKey(k)) {
                    cache[k]
                } else {
                    val t = Tile(p)
                    cache[k] = t
                    t
                }
                nwT!!.seT = this
            }
            return nwT!!
        }

        private fun getSouthEastTile(cache: MutableMap<String, Tile>): Tile {
            if (seT == null) {
                val p = Pos(pos.x, pos.y - 1, pos.z + 1)
                val k = p.toString()
                seT = if (cache.containsKey(k)) {
                    cache[k]
                } else {
                    val t = Tile(p)
                    cache[k] = t
                    t
                }
                seT!!.nwT = this
            }
            return seT!!
        }

        private fun getNorthEastTile(cache: MutableMap<String, Tile>): Tile {
            if (neT == null) {
                val p = Pos(pos.x + 1, pos.y, pos.z - 1)
                val k = p.toString()
                neT = if (cache.containsKey(k)) {
                    cache[k]
                } else {
                    val t = Tile(p)
                    cache[k] = t
                    t
                }
                neT!!.swT = this
            }
            return neT!!
        }

        var visited = false

        private var wT: Tile? = null
        private var eT: Tile? = null
        private var swT: Tile? = null
        private var nwT: Tile? = null
        private var seT: Tile? = null
        private var neT: Tile? = null

        fun countBlackTiles(): Int {
            if (visited) {
                return 0
            }

            visited = true

            var ret = 0

            wT?.let { ret += it.countBlackTiles() }
            eT?.let { ret += it.countBlackTiles() }
            swT?.let { ret += it.countBlackTiles() }
            nwT?.let { ret += it.countBlackTiles() }
            seT?.let { ret += it.countBlackTiles() }
            neT?.let { ret += it.countBlackTiles() }

            if (isBlack) {
                ret++
            }

            return ret
        }

        fun flip() {
            isBlack = !isBlack
        }

        protected fun flipInternal(instructions: String,
                         index: Int,
                         cache: MutableMap<String, Tile>) {

            val key = pos.toString()
            if (!cache.containsKey(key)) {
                cache[key] = this
            }

            if (index > instructions.lastIndex) {
                flip()
                return
            }

            when {
                instructions[index] == 'e' -> getEastTile(cache).flipInternal(instructions, index + 1, cache)
                instructions[index] == 'w' -> getWestTile(cache).flipInternal(instructions, index + 1, cache)
                instructions[index] == 'n' -> {
                    if (instructions[index + 1] == 'e') {
                        getNorthEastTile(cache).flipInternal(instructions, index + 2, cache)
                    } else if (instructions[index + 1] == 'w') {
                        getNorthWestTile(cache).flipInternal(instructions, index + 2, cache)
                    }
                }
                instructions[index] == 's' -> {
                    if (instructions[index + 1] == 'e') {
                        getSouthEastTile(cache).flipInternal(instructions, index + 2, cache)
                    } else if (instructions[index + 1] == 'w') {
                        getSouthWestTile(cache).flipInternal(instructions, index + 2, cache)
                    }
                }
            }
        }

        fun runExhibit(cache: MutableMap<String, Tile>): List<Tile> {
//            if (visited) {
//                return listOf()
//            }

            val ret = mutableListOf<Tile>()

            if (shouldFlip(cache)) {
                ret.add(this)
            }

//            visited = true

            val westTile = getWestTile(cache)
            if (westTile.shouldFlip(cache)) {
                ret.add(westTile)
            }

            val eastTile = getEastTile(cache)
            if (eastTile.shouldFlip(cache)) {
                ret.add(eastTile)
            }

            val southWestTile = getSouthWestTile(cache)
            if (southWestTile.shouldFlip(cache)) {
                ret.add(southWestTile)
            }

            val southEastTile = getSouthEastTile(cache)
            if (southEastTile.shouldFlip(cache)) {
                ret.add(southEastTile)
            }

            val northEastTile = getNorthEastTile(cache)
            if (northEastTile.shouldFlip(cache)) {
                ret.add(northEastTile)
            }

            val northWestTile = getNorthWestTile(cache)
            if (northWestTile.shouldFlip(cache)) {
                ret.add(northWestTile)
            }

            return ret
        }

        private fun shouldFlip(cache: MutableMap<String, Tile>): Boolean {
            if (visited) {
                return false
            }
            visited = true

            val numberOfBlackTileNeighbours = getNumberOfBlackTileNeighbours(cache)

            if (isBlack && ((numberOfBlackTileNeighbours == 0) || (numberOfBlackTileNeighbours > 2))) {
                return true
            } else if (!isBlack && (numberOfBlackTileNeighbours == 2)) {
                return true
            }

            return false
        }

        private fun getNumberOfBlackTileNeighbours(cache: MutableMap<String, Tile>): Int {

            var ret = 0

            if (getWestTile(cache).isBlack) { ret++ }
            if (getEastTile(cache).isBlack) { ret++ }
            if (getSouthWestTile(cache).isBlack) { ret++ }
            if (getNorthWestTile(cache).isBlack) { ret++ }
            if (getSouthEastTile(cache).isBlack) { ret++ }
            if (getNorthEastTile(cache).isBlack) { ret++ }

            return ret
        }
    }

    private class Pos(val x: Int, val y: Int, val z: Int) {
        override fun toString(): String {
            return "$x,$y,$z"
        }
    }
}