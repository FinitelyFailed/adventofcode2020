package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log


class Day23: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val cups0 = getInput(context, "day23_test")

        val result0 = cups0.run(10, true)

        val test0 = result0 == "192658374"

        val cups1 = getInput(context, "day23_test")

        val result1 = cups1.run(100, true)

        val test1 = result1 == "167384529"

        return test0 && test1
    }

    override fun testAnswer02(context: Context): Boolean {
        val cups = getInput2(context, "day23_test")

        val result = cups.run(10000000, false)

        val test = result == "149245887792"

        return test
    }

    override fun getAnswer01(context: Context): String {
        val cups = getInput(context, "day23")

        val result = cups.run(100, true)

        return result.substring(1)
    }

    override fun getAnswer02(context: Context): String {
        val cups = getInput2(context, "day23")

        val result = cups.run(10000000, false)

        return result
    }

    private fun getInput(context: Context, filename: String): Cups {
        context.assets.open(filename).bufferedReader().use {
            val line = it.readLine()
            val list = mutableListOf<Int>()
            line.forEach { c ->
                if (Character.isDigit(c)) {
                    list.add(Character.getNumericValue(c))
                }
            }

            return Cups(list.toIntArray())
        }
    }


    private fun getInput2(context: Context, filename: String): Cups {
        val ret = IntArray(1000000) { -1 }

        context.assets.open(filename).bufferedReader().use {
            val line = it.readLine()
            line.forEachIndexed { index, c ->
                if (Character.isDigit(c)) {
                    ret[index] = Character.getNumericValue(c)
                }
            }

            var value = Int.MIN_VALUE
            for (i in ret.indices) {
                if (ret[i] == -1) {
                    value++
                    ret[i] = value
                } else {
                    if (ret[i] > value) {
                        value = ret[i]
                    }
                }
            }
        }

        return Cups(ret)
    }

    companion object {
        private const val TAG: String = "Day23"
    }

    private class Cups(data: IntArray) {

        var initialCup: Cup

        val valueAsIndex = mutableMapOf<Int, Cup>()

        val size = data.size

        val minLabel = data.minOf { it }

        val maxLabel = data.maxOf { it }

        init {
            initialCup = createCups(data)
        }

        fun run(turns: Int, returnAll: Boolean): String  {

            var currentCup: Cup = initialCup
            val pickedCups = Array<Cup?>(3) { null }
            for (turn in 1..turns) {

                //Log.d(TAG, "-- move $turn --")

                //Log.d(TAG, "cups: ${toString(false)}")

                // Pick cups
                for (i in pickedCups.indices) {
                    pickedCups[i] = currentCup.next(i)
                }

//                Log.d(TAG, "pick up: ${pickedCups.joinToString(", ") {
//                    it!!.value.toString()
//                }}")

                // Find a destination cups
                val destinationCup = getDestinationCup(currentCup.value, pickedCups)

//                Log.d(TAG, "destination: ${destinationCup.value}")

                plugHoleInList(currentCup, pickedCups.last()!!.next!!)

                insertPickedCups(destinationCup, pickedCups)

                // Go to next cup
                currentCup = currentCup.next!!


//                Log.d(TAG, "cups result: ${toString(false)}")
            }

            return if (returnAll) {
                toString(true)
            } else {
                val oneCup = findIndexOf(1)
                val val0 = oneCup.next!!.value.toLong()
                val val1 = oneCup.next!!.next!!.value.toLong()
                return (val0 * val1).toString()
            }
        }

        override fun toString(): String {
            return toString(false)
        }

        fun toString(plain: Boolean = false): String {
            if (plain) {

                val cupOne = findIndexOf(1)

                var ret = ""
                var currentCup = cupOne
                do {
                    ret += currentCup.value
                    currentCup = currentCup.next!!
                } while (cupOne != currentCup)

                return ret
            } else {
                val cupOne = findIndexOf(1)

                var ret = ""
                var currentCup = cupOne
                do {
                    ret += "${currentCup.value}, "
                    currentCup = currentCup.next!!
                } while (cupOne != currentCup)

                return ret
            }
        }

        fun findIndexOf(value: Int): Cup {
            return valueAsIndex[value]!!
        }

        private fun insertPickedCups(cup: Cup, pickedCups: Array<Cup?>) {
            val first: Cup = pickedCups.first()!!
            val last: Cup = pickedCups.last()!!

            cup.next!!.previous = last
            last.next = cup.next

            cup.next = first
            first.previous = cup
        }

        private fun plugHoleInList(cup0: Cup, cup1: Cup) {
            cup0.next = cup1
            cup1.previous = cup0
        }

        private fun getDestinationCup(value: Int, pickedCups: Array<Cup?>): Cup {
            var searchValue = value

            while (true) {
                for (i in pickedCups.indices) {
                    searchValue--
                    if (searchValue <= 0) {
                        searchValue = maxLabel
                    }

                    if (searchValue != value && pickedCups.find { it!!.value == searchValue } == null) {
                        return valueAsIndex[searchValue]!!
                    }
                }
            }
        }

        private fun createCups(data: IntArray): Cup {
            var firstCup: Cup? = null
            var lastCup: Cup? = null
            var currentCup: Cup? = null
            for (i in data.indices) {
                val cup = Cup(data[i])
                cup.previous = currentCup
                currentCup?.next = cup
                currentCup = cup

                if (i == 0) {
                    firstCup = cup
                } else if (i == data.lastIndex) {
                    lastCup = cup
                }

                valueAsIndex[currentCup.value] = currentCup
            }

            firstCup?.previous = lastCup
            lastCup?.next = firstCup

            return firstCup!!
        }
    }

    private class Cup(val value: Int) {
        var previous: Cup? = null
        var next: Cup? = null

        fun next(steps: Int): Cup {
            return if (steps == 0) {
                next!!
            } else {
                next!!.next(steps - 1)
            }
        }
    }
}