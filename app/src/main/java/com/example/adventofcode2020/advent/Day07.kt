package com.example.adventofcode2020.advent

import android.content.Context

class Day07: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        return 4 == findBag(getInputs(context, "day07_test"), listOf("shiny gold"))
    }

    override fun getAnswer01(context: Context): String {
        return findBag(getInputs(context, "day07"), listOf("shiny gold")).toString()
    }

    override fun testAnswer02(context: Context): Boolean {
        val first = findTotalNumOfBags(getInputs(context, "day07_test"), "shiny gold") - 1
        val second = findTotalNumOfBags(getInputs(context, "day07_test_01"), "shiny gold") - 1
        return 32 == first && 126 == second
    }

    override fun getAnswer02(context: Context): String {
        return (findTotalNumOfBags(getInputs(context, "day07"), "shiny gold") - 1).toString()
    }

    private fun findTotalNumOfBags(rules: List<String>, bagToFind: String): Int {
        val regex: String = "($bagToFind).*contain"
        val fulfilledRule = rules.find {
            it.contains(regex.toRegex())
        }

        return if (fulfilledRule == null) {
            0
        } else {
            if (fulfilledRule.endsWith("no other bags.")) {
                1
            } else {
                1 + handleRule(rules.filter { it != fulfilledRule }, fulfilledRule.split("contain")[1].trim())
            }
        }
    }

    private fun handleRule(rules: List<String>, rule: String): Int {
        val parts = rule.split(",")

        var ret = 0

        val regex: String = "([a-z]+ [a-z]+(?<!\\ bag))"
        parts.forEach { part ->
            val numS: String = part.trim().split(' ')[0]
            val num: Int = numS.toInt()
            val bag: String = regex.toRegex().find(part)!!.value

            ret += num * findTotalNumOfBags(rules, bag)
        }
        return ret
    }

    private fun findBag(rules: List<String>, bagsToFind: List<String>): Int {
        val regex: String = "contain.*(${bagsToFind.joinToString("|")})"
        val fulfilledRules = rules.filter {
            it.contains(regex.toRegex())
        }

        if (fulfilledRules.isEmpty()) {
            return 0
        }

        val newRules = rules.filter { !fulfilledRules.contains(it) }
        val fulfilledRulesBags = fulfilledRules.map { it.split("bags")[0].trim() }

        return fulfilledRules.size + findBag(newRules, fulfilledRulesBags)
    }

    private fun getInputs(context: Context, fileName: String): List<String> {
        val rules: MutableList<String> = mutableListOf()
        context.assets.open(fileName).bufferedReader().use {
            it.forEachLine { ruleLine ->
                rules.add(ruleLine)
            }
        }
        return rules
    }
}