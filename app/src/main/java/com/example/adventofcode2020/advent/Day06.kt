package com.example.adventofcode2020.advent

import android.content.Context

class Day06: BaseDay {

    override fun testAnswer01(context: Context): Boolean {
        return true
    }

    override fun testAnswer02(context: Context): Boolean {
        return true
    }

    override fun getAnswer01(context: Context): String {
        //return getSum(context, "day06_test").toString()
        return getOrSum(context, "day06").toString()
    }

    override fun getAnswer02(context: Context): String {
        //return getAndSum(context, "day06_test").toString()
        return getAndSum(context, "day06").toString()
    }

    private fun getOrSum(context: Context, fileName: String): Int {
        val groups: List<Group> = getInput(context, fileName)

        var num: Int = 0
        groups.forEach { group ->
            num += group.yesAnswers.size
        }

        return num
    }

    private fun getAndSum(context: Context, fileName: String): Int {
        val groups: List<Group> = getInput(context, fileName)

        var num: Int = 0
        groups.forEach { group ->
            num += group.getNumOfAnswers()
        }

        return num
    }

    private fun getInput(context: Context, fileName: String): List<Group> {
        val groups: MutableList<Group> = mutableListOf()
        context.assets.open(fileName).bufferedReader().use {
            val groupData: MutableList<String> = mutableListOf()
            it.forEachLine { line ->
                if (line.isNullOrEmpty()) {
                    groups.add(Group(groupData))
                    groupData.clear()
                } else {
                    groupData.add(line)
                }
            }

            if (groupData.isNotEmpty()) {
                groups.add(Group(groupData))
            }
        }
        return groups
    }

    private class Group(data: List<String>) {
        val numOfLines: Int = data.size
        val yesAnswers: Set<Char> = getYesAnswers(data)
        private val answers: Map<Char, Int> = getAnswers(data)

        fun getNumOfAnswers(): Int {
            return answers.filter {
                it.value == numOfLines
            }.size
        }

        private fun getYesAnswers(data: List<String>): Set<Char> {
            val ret: MutableSet<Char> = mutableSetOf()
            data.forEach { line ->
                line.forEach {
                    ret.add(it)
                }
            }
            return ret
        }

        private fun getAnswers(data: List<String>): Map<Char, Int> {
            val ret: MutableMap<Char, Int> = mutableMapOf()
            data.forEach { line ->
                line.forEach { char ->
                    if (ret.containsKey(char)) {
                        ret[char]?.let {
                            ret[char] = it + 1
                        }
                    } else {
                        ret[char] = 1
                    }
                }
            }
            return ret
        }
    }
}