package com.example.adventofcode2020.advent

import android.content.Context

class Day08: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val program = getInput(context, "day08_test")

        val cpu = Cpu(program)
        cpu.run()
        return cpu.accumulator == 5
    }

    override fun testAnswer02(context: Context): Boolean {
        val program = getInput(context, "day08_test")

        val cpu = Cpu(program)
        cpu.run(true)
        return cpu.accumulator == 8
    }

    override fun getAnswer01(context: Context): String {
        val program = getInput(context, "day08")

        val cpu = Cpu(program)
        cpu.run()
        return cpu.accumulator.toString()
    }

    override fun getAnswer02(context: Context): String {
        val program = getInput(context, "day08")

        val cpu = Cpu(program)
        cpu.run(true)
        return cpu.accumulator.toString()
    }

    private fun getInput(context: Context, filename: String): List<String> {
        context.assets.open(filename).bufferedReader().use {
            return it.readLines()
        }
    }

    private class Cpu(private val program: List<String>) {
        var accumulator: Int = 0
            private set
        private var pc: Int = 0
        private val stacktrace: MutableList<Int> = mutableListOf()

        fun run(withFix: Boolean = false) {
            if (withFix) {

                val intToToggle = getToggableIndices(program)

                intToToggle.forEach { indexOfToggleInst ->
                    while (!stacktrace.contains(pc)) {
                        if (pc >= program.size) {
                            return
                        }

                        stacktrace.add(pc)
                        if (pc == indexOfToggleInst) {
                            pc += runInstruction(toggleInstruction(program[pc]))
                        } else {
                            pc += runInstruction(program[pc])
                        }
                    }
                    reset()
                }
            } else {
                while (!stacktrace.contains(pc)) {
                    stacktrace.add(pc)
                    pc += runInstruction(program[pc])
                }
            }
        }

        private fun reset() {
            stacktrace.clear()
            pc = 0
            accumulator = 0
        }

        private fun getToggableIndices(program: List<String>): List<Int> {
            val indices: MutableList<Int> = mutableListOf()

            program.forEachIndexed { index, s ->
                if (!s.contains("acc")) {
                    indices.add(index)
                }
            }
            return indices
        }

        private fun toggleInstruction(instruction: String): String {
            if (instruction.contains("nop")) {
                return instruction.replace("nop", "jmp")
            } else if (instruction.contains("jmp")) {
                return instruction.replace("jmp", "nop")
            }
            return instruction
        }

        private fun runInstruction(instruction: String): Int {
            val parts = instruction.split(" ")
            val inst: String = parts[0].trim()
            val value: String = parts[1].trim()

            when (inst) {
                "nop" -> return runNop()
                "acc" -> return runAcc(value)
                "jmp" -> return runJmp(value)
            }
            return 1
        }

        private fun runNop(): Int = 1

        private fun runAcc(value: String): Int {
            accumulator += getNum(value)
            return 1
        }

        private fun runJmp(value: String): Int {
            return getNum(value)
        }

        private fun getNum(value: String): Int {
            return if (value.startsWith("+")) {
                value.substring(1).toInt()
            } else {
                -1 * value.substring(1).toInt()
            }
        }
    }

}