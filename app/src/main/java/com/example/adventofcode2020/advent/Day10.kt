package com.example.adventofcode2020.advent

import android.content.Context

class Day10: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val input1: Array<Int> = getInput(context, "day10_test")
        val output1 = solve1(input1)

        val input2: Array<Int> = getInput(context, "day10_test_01")
        val output2 = solve1(input2)

        return output1[1] == 7 &&
                output1[3] == 5 &&
                output2[1] == 22 &&
                output2[3] == 10
    }

    override fun testAnswer02(context: Context): Boolean {
        val input1: Array<Int> = getInput(context, "day10_test", true)
        val input2: Array<Int> = getInput(context, "day10_test_01", true)

        val outputNum1 = solve2(input1)
        val outputNum2 = solve2(input2)

        return outputNum1 == 8L && outputNum2 == 19208L
    }

    override fun getAnswer01(context: Context): String {
        val input: Array<Int> = getInput(context, "day10")
        val output = solve1(input)

        output[1]?.let { numOf1 ->
            output[3]?.let { numOf3 ->
                return (numOf1 * numOf3).toString()
            }
        }
        return ""
    }

    override fun getAnswer02(context: Context): String {
        val input1: Array<Int> = getInput(context, "day10", true)
        val output1 = solve2(input1)

        return output1.toString()
    }

    private fun solve1(adapters: Array<Int>): Map<Int, Int> {
        val output: MutableMap<Int, Int> = mutableMapOf()
        var current = 0
        adapters.forEach {
            val diff = it - current

            val i: Int? = output[diff]
            if (i == null) {
                output[diff] = 1
            } else {
                output[diff] = 1 + i
            }

            current = it
        }

        output[3]?.let {
            output[3] = it + 1
        }

        return output
    }

    private fun solve2(adapters: Array<Int>): Long {
        // Create empty cache.
        val cache: MutableMap<Int, Long> = mutableMapOf()

        // Call inner func.
        return innerSolve2(adapters, 0, cache)
    }

    private fun innerSolve2(adapters: Array<Int>, currentIndex: Int, cache: MutableMap<Int, Long>): Long {

        // If the this index is already solved, return its solution.
        if (cache.containsKey(currentIndex)) {
            cache[currentIndex]?.let {
                return it
            }
        }

        // At a leaf, return 1 and save in cache for the current index.
        if (currentIndex == adapters.size - 1) {
            cache[currentIndex] = 1
            return 1
        }

        // Recursive for the next (up to 3) values.
        var ret: Long = 0
        for (i in (currentIndex + 1) until adapters.size) {
            if (adapters[i] <= adapters[currentIndex] + 3) {
                ret += innerSolve2(adapters, i, cache)
            } else {
                break
            }
        }

        // Save in cache and return sum.
        cache[currentIndex] = ret
        return ret
    }

    private fun getInput(context: Context, filename: String, addStartAndEnd: Boolean = false): Array<Int> {
        val ret: MutableList<Int> = mutableListOf()

        if (addStartAndEnd) {
            ret.add(0)
        }

        context.assets.open(filename).bufferedReader().use {
            it.forEachLine { line ->
                val num: Int? = line.toIntOrNull()
                if (num != null) {
                    ret.add(num)
                }
            }
        }

        ret.sort()

        if (addStartAndEnd) {
            ret.add(ret.last() + 3)
        }

        return ret.toTypedArray()
    }
}