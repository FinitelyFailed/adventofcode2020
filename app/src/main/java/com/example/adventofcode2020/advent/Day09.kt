package com.example.adventofcode2020.advent

import android.content.Context
import kotlin.math.max
import kotlin.math.min

class Day09: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val input: Array<Long> = getInput(context, "day09_test")

        val invalidNumber: Long = findInvalidNumber(input, 5)
        return invalidNumber == 127L
    }

    override fun testAnswer02(context: Context): Boolean {
        val input: Array<Long> = getInput(context, "day09_test")

        val weakness: Long = findWeakness(input, 5)

        return weakness == 62L
    }

    override fun getAnswer01(context: Context): String {
        val input: Array<Long> = getInput(context, "day09")

        val invalidNumber: Long = findInvalidNumber(input, 25)
        return invalidNumber.toString()
    }

    override fun getAnswer02(context: Context): String {
        val input: Array<Long> = getInput(context, "day09")

        val weakness: Long = findWeakness(input, 25)

        return weakness.toString()
    }

    private fun findWeakness(data: Array<Long>, preambleLength: Int): Long {
        val invalidNumIndex: Int = findIndexOfInvalidNumber(data, preambleLength)

        val minMax = findSum(data, invalidNumIndex)

        return minMax.first + minMax.second
    }

    private fun findSum(data: Array<Long>, invalidNumIndex: Int): Pair<Long, Long> {

        var minValue: Long = Long.MAX_VALUE
        var maxValue: Long = Long.MIN_VALUE

        var tmpValue: Long = 0

        for (i in 0..data.size) {
            tmpValue += data[i]
            minValue = min(minValue, data[i])
            maxValue = max(maxValue, data[i])

            innerLoop@ for (j in (i + 1)..data.size) {
                tmpValue += data[j]
                minValue = min(minValue, data[j])
                maxValue = max(maxValue, data[j])

                if (tmpValue == data[invalidNumIndex]) {
                    return Pair<Long, Long>(minValue, maxValue)
                } else if (tmpValue > data[invalidNumIndex]) {
                    break@innerLoop
                }
            }

            minValue = Long.MAX_VALUE
            maxValue = Long.MIN_VALUE
            tmpValue = 0
        }
        return Pair<Long, Long>(minValue, maxValue)
    }

    private fun findInvalidNumber(data: Array<Long>, preambleLength: Int): Long {
        val index: Int = findIndexOfInvalidNumber(data, preambleLength)
        if (index > 0 && index < data.size) {
            return data[index]
        }
        return -1
    }

    private fun findIndexOfInvalidNumber(data: Array<Long>, preambleLength: Int): Int {
        var preambleStartIndex = 0
        var preambleEndIndex: Int = preambleLength - 1
        (preambleLength..data.size).forEach { i ->
            if (!isValid(preambleStartIndex, preambleEndIndex, i, data)) {
                return i
            }

            preambleStartIndex += 1
            preambleEndIndex += 1
        }
        return -1
    }

    private fun isValid(preambleStartIndex: Int,
                        preambleEndIndex: Int,
                        currentIndex: Int,
                        data: Array<Long>): Boolean {

        var tmp: Long
        (preambleStartIndex..preambleEndIndex).forEach { i ->
            tmp = data[currentIndex] - data[i]

            (preambleStartIndex..preambleEndIndex).forEach { j ->
                if (i != j) {
                    if (tmp == data[j]) {
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun getInput(context: Context, filename: String): Array<Long> {
        val ret: MutableList<Long> = mutableListOf()
        context.assets.open(filename).bufferedReader().use {
            it.forEachLine {
                val num: Long? = it.toLongOrNull()
                num?.let {
                    ret.add(num)
                }
            }
        }
        return ret.toTypedArray()
    }
}