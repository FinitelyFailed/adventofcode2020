package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log

class Day15: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val input = getInput(context, "day15_test")
        val expectedResults = intArrayOf(436, 1, 10, 27, 78, 438, 1836)

        for (i in input.indices) {
            val game = Game(input[i])
            val solution = game.run(2020)
            if (solution != expectedResults[i]) {
                return false
            }
        }
        return true
    }

    override fun testAnswer02(context: Context): Boolean {
        val input = getInput(context, "day15_test")
        val expectedResults = intArrayOf(175594, 2578, 3544142, 261214, 6895259, 18, 362)

        for (i in input.indices) {
            val game = Game(input[i])
            val solution = game.run(30000000)
            if (solution != expectedResults[i]) {
                return false
            }
        }
        return true
    }

    override fun getAnswer01(context: Context): String {
        val input = getInput(context, "day15")

        val game = Game(input.first())
        val solution = game.run(2020)
        return solution.toString()
    }

    override fun getAnswer02(context: Context): String {
        val input = getInput(context, "day15")

        val game = Game(input.first())
        val solution = game.run(30000000)
        return solution.toString()
    }

    private fun getInput(context: Context, filename: String): List<IntArray> {
        context.assets.open(filename).bufferedReader().use {
            val ret = mutableListOf<IntArray>()
            it.forEachLine { line ->
                 ret.add(line.split(',').map { num -> num.toInt() }.toIntArray())
            }
            return ret
        }
    }

    private class Game(val startNumbers: IntArray) {

        private var history = intArrayOf()

        fun run(numOfTurns: Int): Int {
            history = IntArray(numOfTurns) { 0 }
            var spokenNumber = 0
            var prevSpokenNumber = 0
            for (i in 1..numOfTurns) {
                spokenNumber = if (i <= startNumbers.size) {
                    startNumbers[i - 1]
                } else {
                    speak(spokenNumber, i)
                }
                //Log.v(TAG, "Spoke: $spokenNumber and prev: $prevSpokenNumber at turn $i")
                addToHistory(prevSpokenNumber, i - 1)
                prevSpokenNumber = spokenNumber
            }
            return spokenNumber
        }

        private fun speak(number: Int, turn: Int): Int {
            return if (wasFirstTimeSpoken(number)) {
                0
            } else {
                getAge(number, turn)
            }
        }

        private fun getAge(number: Int, turn: Int): Int {
            return turn -  history[number] - 1
        }

        private fun wasFirstTimeSpoken(number: Int): Boolean {
            return history[number] == 0
        }

        private fun addToHistory(number: Int, turn: Int) {
            history[number] = turn
        }

        companion object {
            private const val TAG: String = "Game"
        }
    }
}