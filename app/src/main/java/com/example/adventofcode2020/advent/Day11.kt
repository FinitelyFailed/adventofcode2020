package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log
import kotlin.math.max
import kotlin.math.min

class Day11: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val testInput = getInput(context, "day11_test")
        val testResult = getInput(context, "day11_test_result")

        //Log.v(TAG, "TestInput ${testInput.toString()}")
        //Log.v(TAG, "TestResult ${testResult.toString()}")

        var running = true
        while (running) {
            running = testInput.runTurn(false)
        }

        return testInput.isEqual(testResult)
    }

    override fun testAnswer02(context: Context): Boolean {
        val testInput = getInput(context, "day11_test")
        val testResult = getInput(context, "day11_test_result_part_2")

        //Log.v(TAG, "TestInput2 ${testInput.toString()}")
        //Log.v(TAG, "TestResult2 ${testResult.toString()}")

        var running = true
        while (running) {
            running = testInput.runTurn2(false)
        }

        return testInput.isEqual(testResult)
    }

    override fun getAnswer01(context: Context): String {
        val input = getInput(context, "day11")

        //Log.v(TAG, "Input ${input.toString()}")

        var running = true
        while (running) {
            running = input.runTurn(false)
        }

        return input.getNumOccupiedSeats().toString()
    }

    override fun getAnswer02(context: Context): String {
        val input = getInput(context, "day11")

        //Log.v(TAG, "Input ${input.toString()}")

        var running = true
        while (running) {
            running = input.runTurn2(false)
        }

        return input.getNumOccupiedSeats().toString()
    }

    private fun getInput(context: Context, filename: String): SeatingArea {
        context.assets.open(filename).bufferedReader().use {
            return SeatingArea(it.readLines())
        }
    }

    private class SeatingArea(data: List<String>) {

        var area: Array<CharArray> = initArea(data)
            private set

        private fun initArea(data: List<String>): Array<CharArray> {
            val ret = Array(data.size) { CharArray(data.first().length) { '.' } }

            for (row in data.indices) {
                for (col in data[row].indices) {
                    ret[row][col] = data[row][col]
                }
            }

            return ret
        }

        fun getNumOccupiedSeats(): Int {
            var ret = 0
            for (row in area.indices) {
                for (col in area[row].indices) {
                    if (area[row][col] == '#') {
                        ret++
                    }
                }
            }
            return ret
        }

        fun runTurn(logArea: Boolean): Boolean {
            val ret: Array<CharArray> =  Array(area.size) { CharArray(area.first().size) { '.' } }

            var change = false

            area.forEachIndexed { rowIndex, row ->

                row.forEachIndexed { colIndex, space ->
                    ret[rowIndex][colIndex] = area[rowIndex][colIndex]

                    if (space == '#') {
                        if (shouldLeave(area, rowIndex, colIndex, 4)) {
                            ret[rowIndex][colIndex] = 'L'
                            change = true
                        }
                    } else {
                        if (shouldOccupy(area, rowIndex, colIndex)) {
                            ret[rowIndex][colIndex] = '#'
                            change = true
                        }
                    }
                }
            }

            area = ret
            if (logArea) {
                Log.v(TAG, "RunTurn ${toString()}")
            }

            return change
        }

        fun runTurn2(logArea: Boolean): Boolean {
            val ret: Array<CharArray> =  Array(area.size) { CharArray(area.first().size) { '.' } }

            var change = false

            area.forEachIndexed { rowIndex, row ->

                row.forEachIndexed { colIndex, space ->
                    ret[rowIndex][colIndex] = area[rowIndex][colIndex]

                    if (space == '#') {
                        if (shouldLeave2(area, rowIndex, colIndex, 5)) {
                            ret[rowIndex][colIndex] = 'L'
                            change = true
                        }
                    } else {
                        if (shouldOccupy2(area, rowIndex, colIndex)) {
                            ret[rowIndex][colIndex] = '#'
                            change = true
                        }
                    }
                }
            }

            area = ret
            if (logArea) {
                Log.v(TAG, "RunTurn ${toString()}")
            }

            return change
        }

        fun isEqual(otherSeatingArea: SeatingArea): Boolean {
            for (row in area.indices) {
                for (col in area[row].indices) {
                    if (area[row][col] != otherSeatingArea.area[row][col]) {
                        return false
                    }
                }
            }
            return true
        }

        override fun toString(): String {
            var ret = "\n"
            area.forEach {
                ret += "${String(it)}\n"
            }
            return ret
        }

        private fun shouldOccupy(area: Array<CharArray>, rowIndex: Int, colIndex: Int): Boolean {
            if (area[rowIndex][colIndex] != 'L') {
                return false
            }

            val rowStart: Int = max(rowIndex - 1, 0)
            val rowEnd: Int = min(rowIndex + 1, area.size - 1)

            val colStart: Int = max(colIndex - 1, 0)
            val colEnd: Int = min(colIndex + 1, area.first().size - 1)

            for (row in rowStart..rowEnd) {
                for (col in colStart..colEnd) {
                    if (!(row == rowIndex && col == colIndex)) {
                        if (isOccupied(area, row, col)) {
                            return false
                        }
                    }
                }
            }

            return true
        }

        private fun shouldOccupy2(area: Array<CharArray>, rowIndex: Int, colIndex: Int): Boolean {
            if (area[rowIndex][colIndex] != 'L') {
                return false
            }

            for (rowDir in -1..1) {
                for (colDir in -1..1) {
                    if (rowDir == 0 && colDir == 0) {
                        continue
                    }

                    if (doesDirectionContain(area, rowIndex, colIndex, rowDir, colDir, '#')) {
                        return false
                    }
                }
            }

            return true
        }

        private fun shouldLeave(area: Array<CharArray>,
                                rowIndex: Int,
                                colIndex: Int,
                                occupiedNeighboursThreshold: Int): Boolean {
            if (area[rowIndex][colIndex] != '#') {
                return false
            }

            val rowStart: Int = max(rowIndex - 1, 0)
            val rowEnd: Int = min(rowIndex + 1, area.size - 1)

            val colStart: Int = max(colIndex - 1, 0)
            val colEnd: Int = min(colIndex + 1, area.first().size - 1)

            var numOccupiedNeighbors = 0
            for (row in rowStart..rowEnd) {
                for (col in colStart..colEnd) {
                    if (!(row == rowIndex && col == colIndex)) {
                        if (isOccupied(area, row, col)) {
                            numOccupiedNeighbors++

                            if (numOccupiedNeighbors >= occupiedNeighboursThreshold) {
                                return true
                            }
                        }
                    }
                }
            }

            return false
        }

        private fun shouldLeave2(area: Array<CharArray>,
                                 rowIndex: Int,
                                 colIndex: Int,
                                 seenOccupiedSeatsThreshold: Int): Boolean {
            if (area[rowIndex][colIndex] != '#') {
                return false
            }

            var numOccupiedNeighbors = 0

            for (rowDir in -1..1) {
                for (colDir in -1..1) {
                    if (rowDir == 0 && colDir == 0) {
                        continue
                    }

                    if (doesDirectionContain(area, rowIndex, colIndex, rowDir, colDir, '#')) {
                        numOccupiedNeighbors++

                        if (numOccupiedNeighbors >= seenOccupiedSeatsThreshold) {
                            return true
                        }
                    }
                }
            }

            return false
        }

        private fun doesDirectionContain(area: Array<CharArray>,
                                         row: Int,
                                         col: Int,
                                         rowDir: Int,
                                         colDir: Int,
                                         spaceType: Char): Boolean {

            var rowStep = row + rowDir
            var colStep = col + colDir

            while (!isOutsideArea(area, rowStep, colStep)) {
                if (area[rowStep][colStep] == '.') {
                    rowStep += rowDir
                    colStep += colDir
                } else {
                    return area[rowStep][colStep] == spaceType
                }
            }

            return  false
        }

        private fun isOutsideArea(area: Array<CharArray>, row: Int, col: Int): Boolean {

            if (row < 0 || row >= area.size) {
                return true
            }

            if (col < 0 || col >= area[row].size) {
                return true
            }

            return false
        }

        private fun isOccupied(area: Array<CharArray>, rowIndex: Int, colIndex: Int): Boolean {
            return area[rowIndex][colIndex] == '#'
        }

        companion object {
            private const val TAG: String = "Day11 seating area"
        }
    }

    companion object {
        private const val TAG: String = "Day11"
    }
}
