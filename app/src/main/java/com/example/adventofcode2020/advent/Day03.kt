package com.example.adventofcode2020.advent

import android.content.Context

class Day03: BaseDay {

    private val map: MutableList<String> = mutableListOf()
    override fun testAnswer01(context: Context): Boolean {
        return true
    }

    override fun testAnswer02(context: Context): Boolean {
        return true
    }

    override fun getAnswer01(context: Context): String {
        return goDownSlope(getMap(context), 3, 1).toString()
    }

    override fun getAnswer02(context: Context): String {
        val f1 = goDownSlope(getMap(context), 1, 1)
        val f2 = goDownSlope(getMap(context), 3, 1)
        val f3 = goDownSlope(getMap(context), 5, 1)
        val f4 = goDownSlope(getMap(context), 7, 1)
        val f5 = goDownSlope(getMap(context), 1, 2)
        return (f1 * f2 * f3 * f4 * f5).toString()
    }

    private fun goDownSlope(map: List<String>, right: Int, down: Int): Long {
        var col = right
        var row = down

        var numOfThrees: Long = 0

        while (row < map.size) {
            if (isTree(map, col, row)) {
                numOfThrees++
            }

            col += right
            row += down
        }

        return numOfThrees
    }

    private fun isTree(map: List<String>, col: Int, row: Int): Boolean {
        val line: String = map[row]
        val pos: Char = line[col % line.length]
        return pos == '#'
    }

    private fun getMap(context: Context): List<String> {
        if (map.size == 0) {
            loadInput(context)
        }

        return map
    }

    private fun loadInput(context: Context) {
        val fileName = "day03"
        //val fileName = "day03_test"
        context.assets.open(fileName).bufferedReader().use { it ->
            it.forEachLine { line ->
                map.add(line)
            }
        }
    }
}