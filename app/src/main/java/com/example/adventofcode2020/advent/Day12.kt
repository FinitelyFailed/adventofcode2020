package com.example.adventofcode2020.advent

import android.content.Context
import kotlin.math.*

class Day12: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val input = getInput(context, "day12_test")

        val ship = Ship()
        val endPos = ship.run(input)

        val distance = endPos.length()
        return distance == 25
    }

    override fun testAnswer02(context: Context): Boolean {
        val input = getInput(context, "day12_test")

        val ship = Ship()
        val endPos = ship.run2(input)

        val distance = endPos.length()
        return distance == 286
    }

    override fun getAnswer01(context: Context): String {
        val input = getInput(context, "day12")

        val ship = Ship()
        val endPos = ship.run(input)

        return endPos.length().toString()
    }

    override fun getAnswer02(context: Context): String {
        val input = getInput(context, "day12")

        val ship = Ship()
        val endPos = ship.run2(input)

        return endPos.length().toString()
    }

    private fun getInput(context: Context, filename: String): List<String> {
        context.assets.open(filename).bufferedReader().use {
            return it.readLines()
        }
    }

    private class Ship() {
        private var pos: VectorI = VectorI(0, 0)

        private var direction: VectorI = VectorI(1, 0)

        private var waypoint: VectorI = VectorI(10, 1)

        fun run(input: List<String>): VectorI {
            input.forEach { inst ->
                val op = inst.substring(0, 1)
                val value = inst.substring(1, inst.length).toInt()

                when (op) {
                    "N" -> north(value)
                    "S" -> south(value)
                    "E" -> east(value)
                    "W" -> west(value)
                    "L" -> left(value)
                    "R" -> right(value)
                    "F" -> forward(value)
                }
            }

            return pos
        }

        fun run2(input: List<String>): VectorI {
            input.forEach { inst ->
                val op = inst.substring(0, 1)
                val value = inst.substring(1, inst.length).toInt()

                when (op) {
                    "N" -> northWaypoint(value)
                    "S" -> southWaypoint(value)
                    "E" -> eastWaypoint(value)
                    "W" -> westWaypoint(value)
                    "L" -> leftWaypoint(value)
                    "R" -> rightWaypoint(value)
                    "F" -> forwardWaypoint(value)
                }
            }

            return pos
        }

        private fun northWaypoint(distance: Int) {
            waypoint += VectorI(0, distance)
        }

        private fun southWaypoint(distance: Int) {
            waypoint += VectorI(0, -distance)
        }

        private fun eastWaypoint(distance: Int) {
            waypoint += VectorI(distance, 0)
        }

        private fun westWaypoint(distance: Int) {
            waypoint += VectorI(-distance, 0)
        }

        private fun leftWaypoint(degrees: Int) {
            waypoint = waypoint.rotate(-degrees)
        }

        private fun rightWaypoint(degrees: Int) {
            waypoint = waypoint.rotate(degrees)
        }

        private fun forwardWaypoint(distance: Int) {
            pos += waypoint * distance
        }

        private fun forward(distance: Int) {
            pos += (direction * distance)
        }

        private fun north(distance: Int) {
            pos += VectorI(0, distance)
        }

        private fun south(distance: Int) {
            pos += VectorI(0, -distance)
        }

        private fun east(distance: Int) {
            pos += VectorI(distance, 0)
        }

        private fun west(distance: Int) {
            pos += VectorI(-distance, 0)
        }

        private fun right(degrees: Int) {
            direction = direction.rotate(degrees)
        }

        private fun left(degrees: Int) {
            direction = direction.rotate(-degrees)
        }
    }


    private class VectorI(var x: Int, var y: Int) {

        operator fun plus(v: VectorI): VectorI {
            return VectorI(x + v.x, y + v.y)
        }

        operator fun minus(v: VectorI): VectorI {
            return VectorI(x - v.x, y - v.y)
        }

        operator fun times(factor: Int): VectorI {
            return VectorI(x * factor, y * factor)
        }

        fun normalize(): VectorI {
            val l = length()
            return VectorI(x / l, y / l)
        }

        fun length(): Int {
            return abs(x) + abs(y)
        }

        fun rotate(degrees: Int): VectorI {
            when ((degrees % 360)) {
                90 -> return VectorI(y, -x)
                180 -> return VectorI(-x, -y)
                270 -> return VectorI(-y, x)
                -90 -> return VectorI(-y, x)
                -180 -> return VectorI(-x, -y)
                -270 -> return VectorI(y, -x)
            }
            return VectorI(x, y)
        }
    }

    private class Vector(var x: Double, var y: Double) {

        operator fun plus(v: Vector): Vector {
            return Vector(x + v.x, y + v.y)
        }

        operator fun minus(v: Vector): Vector {
            return Vector(x - v.x, y - v.y)
        }

        operator fun times(factor: Double): Vector {
            return Vector(x * factor, y * factor)
        }

        fun normalize(): Vector {
            val l = length()
            return Vector(x / l, y / l)
        }

        fun length(): Double {
            return sqrt(x * x + y * y)
        }

        fun rotate(degrees: Double): Vector {
            val radians = (degrees / 180.0) * PI

            return Vector(x * cos(radians) - y * sin(radians), x * sin(radians) + y * cos(radians))
        }
    }
}