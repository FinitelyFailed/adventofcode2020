package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log

class Day04: BaseDay {

    override fun testAnswer01(context: Context): Boolean {
        return true
    }

    override fun testAnswer02(context: Context): Boolean {
        return true
    }

    override fun getAnswer01(context: Context): String {
        var numOfValidPassports = 0
        loadInput(context).forEach { passport ->
            if (passport.valid) {
                numOfValidPassports++
            }
        }
        return numOfValidPassports.toString()
    }

    override fun getAnswer02(context: Context): String {
        var numOfValidPassports = 0
        loadInput(context).forEach { passport ->
            if (passport.pedanticValid) {
                numOfValidPassports++
            }
        }
        return numOfValidPassports.toString()
    }


    private fun loadInput(context: Context): List<Passport> {
        val passports: MutableList<Passport> = mutableListOf()

        val fileName = "day04"
        //val fileName = "day04_test"
        //val fileName = "day04_test_01"
        //val fileName = "day04_part2_invalid"
        //val fileName = "day04_part2_valid"
        context.assets.open(fileName).bufferedReader().use { it ->
            var passportData: String = ""

            var emptyLines: Int = 0

            it.forEachLine { line ->
                passportData = if (line.isNullOrBlank()) {
                    emptyLines++
                    if (passportData.isNotEmpty()) {
                        passports.add(Passport(passportData))
                    }
                    ""
                } else {
                    if (passportData.isNullOrBlank()) {
                        line
                    } else {
                        "$passportData $line"
                    }
                }
            }
            passports.add(Passport(passportData))
        }

        return passports
    }

    class Passport(data: String) {

        private var birthYear: String? = null
        private var issueYear: String? = null
        private var expirationYear: String? = null
        private var height: String? = null
        private var hairColor: String? = null
        private var eyeColor: String? = null
        private var passportId: String? = null
        private var countryId: String? = null
        var valid: Boolean = false
            private set
        var pedanticValid: Boolean = false
            private set

        init {
            val dataParts = data.split(" ")

            dataParts.forEach { keyValue ->
                val keyValueParts = keyValue.split(":")

                if (keyValueParts.size == 2) {
                    setValue(keyValueParts[0], keyValueParts[1])
                }
            }

            valid = validate()
//            if (!valid) {
//                Log.v(TAG, "NOT VALID: $data")
//            }

            pedanticValid = validatePedantic()
//            if (!pedanticValid) {
//                Log.v(TAG, "NOT PEDANTIC VALID: $data")
//            }
        }

        private fun validate(): Boolean {
            var valid = true
            var missingValues: MutableList<String> = mutableListOf()

            if (birthYear == null) {
                missingValues.add(BIRTH_YEAR_KEY)
                valid = false
            }

            if (issueYear == null) {
                missingValues.add(ISSUE_YEAR_KEY)
                valid = false
            }

            if (expirationYear == null) {
                missingValues.add(EXPIRATION_YEAR_KEY)
                valid = false
            }

            if (height == null) {
                missingValues.add(HEIGHT_KEY)
                valid = false
            }

            if (hairColor == null) {
                missingValues.add(HAIR_COLOR_KEY)
                valid = false
            }

            if (eyeColor == null) {
                missingValues.add(EYE_COLOR_KEY)
                valid = false
            }

            if (passportId == null) {
                missingValues.add(PASSPORT_ID_KEY)
                valid = false
            }

            if (countryId == null) {
                missingValues.add(COUNTRY_ID_KEY)
            }

            //if (missingValues.isNotEmpty()) {
                //Log.v(TAG, "Missing values: ${missingValues.joinToString(",")}")
            //}

            return valid
        }

        private fun validatePedantic(): Boolean {
            var valid = true
            var missingValues: MutableList<String> = mutableListOf()

            if (birthYear == null) {
                missingValues.add(BIRTH_YEAR_KEY)
                valid = false
            } else {
                birthYear?.toIntOrNull()?.let {
                    valid = valid && (it >= 1920) && (it <= 2002)
                }
            }

            if (issueYear == null) {
                missingValues.add(ISSUE_YEAR_KEY)
                valid = false
            } else {
                issueYear?.toIntOrNull()?.let {
                    valid = valid && (it >= 2010) && (it <= 2020)
                }
            }

            if (expirationYear == null) {
                missingValues.add(EXPIRATION_YEAR_KEY)
                valid = false
            } else {
                expirationYear?.toIntOrNull()?.let {
                    valid = valid && (it >= 2020) && (it <= 2030)
                }
            }

            if (height == null) {
                missingValues.add(HEIGHT_KEY)
                valid = false
            } else {
                if (height?.endsWith("cm") == true) {
                    height?.replace("cm", "")?.toIntOrNull()?.let {
                        valid = valid && (it >= 150) && (it <= 193)
                    }
                } else if (height?.endsWith("in") == true) {
                    height?.replace("in", "")?.toIntOrNull()?.let {
                        valid = valid && (it >= 59) && (it <= 76)
                    }
                } else {
                    valid = false
                }
            }

            if (hairColor == null) {
                missingValues.add(HAIR_COLOR_KEY)
                valid = false
            } else {
                valid = valid && hairColor?.matches("#[a-f0-9]{6}".toRegex()) == true
            }

            if (eyeColor == null) {
                missingValues.add(EYE_COLOR_KEY)
                valid = false
            } else {
                valid = valid && VALID_EYE_COLORS.contains(eyeColor)
            }

            if (passportId == null) {
                missingValues.add(PASSPORT_ID_KEY)
                valid = false
            } else {
                valid = valid && passportId?.matches("[0-9]{9}".toRegex()) == true
            }

            if (countryId == null) {
                missingValues.add(COUNTRY_ID_KEY)
            }

//            if (missingValues.isNotEmpty()) {
//                Log.v(TAG, "Missing values: ${missingValues.joinToString(",")}")
//            }

            return valid
        }

        private fun setValue(key: String, value: String) {
            when (key) {
                BIRTH_YEAR_KEY -> birthYear = value
                ISSUE_YEAR_KEY -> issueYear = value
                EXPIRATION_YEAR_KEY -> expirationYear = value
                HEIGHT_KEY -> height = value
                HAIR_COLOR_KEY -> hairColor = value
                EYE_COLOR_KEY -> eyeColor = value
                PASSPORT_ID_KEY -> passportId = value
                COUNTRY_ID_KEY -> countryId = value
            }
        }

        companion object {
            private const val TAG: String = "Day04"
            private const val BIRTH_YEAR_KEY: String = "byr"
            private const val ISSUE_YEAR_KEY: String = "iyr"
            private const val EXPIRATION_YEAR_KEY: String = "eyr"
            private const val HEIGHT_KEY: String = "hgt"
            private const val HAIR_COLOR_KEY: String = "hcl"
            private const val EYE_COLOR_KEY: String = "ecl"
            private const val PASSPORT_ID_KEY: String = "pid"
            private const val COUNTRY_ID_KEY: String = "cid"

            private val VALID_EYE_COLORS: List<String> = listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
        }
    }
}