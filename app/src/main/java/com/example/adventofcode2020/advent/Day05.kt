package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log

class Day05: BaseDay {

    override fun testAnswer01(context: Context): Boolean {
        return true
    }

    override fun testAnswer02(context: Context): Boolean {
        return true
    }

    override fun getAnswer01(context: Context): String {

        //val boardingPasses = getInput(context, "day05_test")
        val boardingPasses = getInput(context, "day05")

        val sortedBoardingPasses = boardingPasses.sortedBy {
            it.id
        }

        return sortedBoardingPasses.last().id.toString()
    }

    override fun getAnswer02(context: Context): String {
        //val boardingPasses = getInput(context, "day05_test")
        val boardingPasses = getInput(context, "day05")

        val sortedBoardingPasses = boardingPasses.sortedBy {
            it.id
        }

        val missingIds: MutableList<Int> = mutableListOf()

        var index: Int = sortedBoardingPasses.first().id
        sortedBoardingPasses.forEach {
            while (index != it.id) {
                missingIds.add(index)
                index++
            }
            index++
        }
        return missingIds.joinToString(",")
    }

    private fun getInput(context: Context, fileName: String): List<BoardingPass> {
        val boardingPasses: MutableList<BoardingPass> = mutableListOf()

        context.assets.open(fileName).bufferedReader().use {
            it.forEachLine { line ->
                boardingPasses.add(BoardingPass(line))
            }
        }

        return boardingPasses
    }

    companion object {
        private const val TAG: String = "Day05"
        private const val LARGEST_ID: Int = 0b1111111111
    }

    private class BoardingPass(private val data: String) {

        private val bitmask: Int = createBitmask(data)
        private val row: Int = bitmask shr 3
        private val col: Int = bitmask and 7
        val id: Int = bitmask

        override fun toString(): String {
            return "$data Row: $row Col: $col Id: $id"
        }

        private fun createBitmask(data: String) : Int {
            var bm: Int = 0
            data.forEach {
                bm = bm shl 1
                if (it == 'B' || it == 'R') {
                    bm = bm or 1
                }
            }
            return bm
        }
    }
}