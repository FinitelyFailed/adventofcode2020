package com.example.adventofcode2020.advent

import android.content.Context
import android.util.Log

@ExperimentalUnsignedTypes
class Day14: BaseDay {

    override fun testAnswer01(context: Context): Boolean {
        val program = getInput(context, "day14_test")

        val solution = program.execute()
        val expectedResult: ULong = 165u
        return solution == expectedResult
    }

    override fun testAnswer02(context: Context): Boolean {
        val program = getInput(context, "day14_test_2")

        val solution = program.execute2()
        val expectedResult: ULong = 208u
        return solution == expectedResult
    }

    override fun getAnswer01(context: Context): String {
        val program = getInput(context, "day14")

        val solution = program.execute()
        return solution.toString()
    }

    override fun getAnswer02(context: Context): String {
        val program = getInput(context, "day14")

        val solution = program.execute2()
        return solution.toString()
    }

    private fun getInput(context: Context, filename: String): Program {
        context.assets.open(filename).bufferedReader().use {
            return Program(it.readLines())
        }
    }

    class Program(private val data: List<String>) {

        private val mem: MutableMap<ULong, ULong> = mutableMapOf()

        fun execute(): ULong {

            var currentMask: Mask? = null
            data.forEach { instruction ->
                if (instruction.startsWith("mem")) {
                    handleMemory(instruction, currentMask)
                } else if (instruction.startsWith("mask")) {
                    currentMask = getMask(instruction)
                }
            }

            var ret: ULong = 0u
            mem.values.forEach {
                ret += it
            }

            return ret
        }

        fun execute2(): ULong {
            var currentMask: Mask2? = null
            data.forEach { instruction ->
                if (instruction.startsWith("mem")) {
                    handleMemory2(instruction, currentMask)
                } else if (instruction.startsWith("mask")) {
                    currentMask = getMask2(instruction)
                }
            }

            var ret: ULong = 0u
            mem.values.forEach {
                ret += it
            }

            return ret
        }

        private fun handleMemory(instruction: String, mask: Mask?) {
            val lastMemIndex = instruction.indexOfFirst { it == ']' }
            val address =  instruction.substring(4, lastMemIndex).toULong()

            val value = applyMask(instruction.substring(lastMemIndex + 4, instruction.length).toULong(), mask)

            mem[address] = value
        }

        private fun handleMemory2(instruction: String, mask: Mask2?) {
            val lastMemIndex = instruction.indexOfFirst { it == ']' }

            val address =  instruction.substring(4, lastMemIndex).toULong()
            val value = instruction.substring(lastMemIndex + 4, instruction.length).toULong()

            mask?.let {
                val addresses = mask.apply(address)

                addresses.forEach { adr ->
                    mem[adr] = value
                }
            }
        }

        private fun getMask(instruction: String): Mask {
            return Mask(instruction.substring(7, instruction.length))
        }

        private fun getMask2(instruction: String): Mask2 {
            return Mask2(instruction.substring(7, instruction.length))
        }

        private fun applyMask(value: ULong, mask: Mask?): ULong {
            return mask?.apply(value) ?: value
        }

        class Mask(mask: String) {
            private val andMask: ULong = createAndMask(mask)
            private val orMask: ULong = createOrMask(mask)

            fun apply(value: ULong): ULong {
                return (value and andMask) or orMask
            }

            private fun createOrMask(mask: String): ULong {
                var m: ULong = 0u
                val oneBit: ULong = 1u
                mask.forEachIndexed { index, c ->
                    if (c == '1') {
                        m = (m or (oneBit shl (mask.length - index - 1)))
                    }
                }

                return m
            }

            private fun createAndMask(mask: String): ULong {
                var m: ULong = ULong.MAX_VALUE
                Log.v(TAG, "createAndMask 1 ${uLongToString(m)}")
                val oneBit: ULong = 1u
                mask.forEachIndexed { index, c ->
                    if (c == '0') {
                        m = (m xor (oneBit shl (mask.length - index - 1)))
                    }
                }

                return m
            }

            private fun uLongToString(value: ULong): String {
                return value.toString(radix = 2)
            }

            companion object {
                private const val TAG: String = "MASK"
            }
        }

        class Mask2(mask: String) {
            private var orMasks =  createOrSubMasks(mask)
            private var andMasks = createSubAndMasks(mask)
            private var globalOrMask = createGlobalOr(mask)
            //private var globalAndMask: ULong = ULong.MAX_VALUE

            fun apply(value: ULong): ULongArray {
                val v: ULong = value or globalOrMask

                val ret = ULongArray(orMasks.size) { 0u }

                for (i in orMasks.indices) {
                    ret[i] = (v or orMasks[i]) and andMasks[i]
                }

//                ret.forEach {
//                    Log.v(TAG, "RET: ${uLongToString(it)}")
//                }

                return ret
            }

            private fun createGlobalOr(mask: String): ULong {
                var ret: ULong = 0u
                val oneBit: ULong = 1u

                for (i in mask.indices) {
                    val shiftPos = mask.length - i -1
                    when (mask[i]) {
                        '1' -> ret = ret or (oneBit shl shiftPos)
                    }
                }

//                Log.v(TAG, "Global or: ${uLongToString(ret)}")

                return ret
            }

            private fun createSubAndMasks(mask: String): ULongArray {
//                val ret =
//                ret.forEach {
//                    Log.v(TAG, "createSubAndMasks: ${uLongToString(it)}")
//                }
                return createSubMasksInner(mask, 0, ULong.MAX_VALUE).toULongArray()
            }

            private fun createOrSubMasks(mask: String): ULongArray {
                val zeroBit: ULong = 0u
//                ret.forEach {
//                    Log.v(TAG, "createOrSubMasks: ${uLongToString(it)}")
//                }
                return createSubMasksInner(mask, 0, zeroBit).toULongArray()
            }

            private fun createSubMasksInner(mask: String, currentIndex: Int, currentValue: ULong): List<ULong> {
                val oneBit: ULong = 1u
                for (i in currentIndex until mask.length) {
                    if (mask[i] == 'X') {
                        val maskIndex = (mask.length - i - 1)

                        val value1 = currentValue and (oneBit shl maskIndex).inv()
                        val value2 = currentValue or (oneBit shl maskIndex)

                        val ret = listOf(createSubMasksInner(mask, i + 1, value1),
                                        createSubMasksInner(mask, i + 1, value2))
                        return ret.flatten()
                    }
                }
                return listOf(currentValue)
            }

            private fun uLongToString(value: ULong): String {
                return value.toString(radix = 2)
            }

            companion object {
                private const val TAG: String = "MASK2"
            }
        }
    }
}