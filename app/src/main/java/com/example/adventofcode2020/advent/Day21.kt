package com.example.adventofcode2020.advent

import android.content.Context

class Day21: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val foods = getInput(context, "day21_test")

        val ingredientsWithoutAllergen = getIngredientsWithoutAllergen(foods)

        return ingredientsWithoutAllergen.size == 5
    }

    override fun testAnswer02(context: Context): Boolean {
        val foods = getInput(context, "day21_test")

        val ingredientsWithAllergens = getIngredientsWithAllergens(foods)

        val csvIngredientsWithAllergens =  ingredientsWithAllergens.joinToString(",")
        return csvIngredientsWithAllergens == "mxmxvkd,sqjhc,fvjkl"
    }

    override fun getAnswer01(context: Context): String {
        val foods = getInput(context, "day21")

        val ingredientsWithoutAllergen = getIngredientsWithoutAllergen(foods)

        return ingredientsWithoutAllergen.size.toString()
    }

    override fun getAnswer02(context: Context): String {
        val foods = getInput(context, "day21")

        val ingredientsWithAllergens = getIngredientsWithAllergens(foods)

        return ingredientsWithAllergens.joinToString(",")
    }

    private fun getIngredientsWithAllergens(foods: List<Food>): List<String> {
        val allergenWithIngredients = getAllergenWithIngredients(foods).toMutableMap()

        val allergenWithIngredient = mutableMapOf<String, String>()

        var allergenToRemove = ""
        var ingredientToRemove = ""
        while (allergenWithIngredients.isNotEmpty()) {
            allergenWithIngredients.forEach {
                if (it.value.size == 1) {
                    allergenToRemove = it.key
                    ingredientToRemove = it.value.first()
                }
            }

            allergenWithIngredient[allergenToRemove] = ingredientToRemove

            allergenWithIngredients.remove(allergenToRemove)

            for (key in allergenWithIngredients.keys) {
                if (allergenWithIngredients[key]!!.contains(ingredientToRemove)) {
                    allergenWithIngredients[key] = allergenWithIngredients[key]!!.filter { it ->
                        it != ingredientToRemove
                    }
                }
            }
        }

        val ret = allergenWithIngredient.toSortedMap().values.toList()

        return ret
    }

    private fun getAllergenWithIngredients(foods: List<Food>): Map<String, List<String>> {
        val allergenList = mutableMapOf<String, List<String>>()

        foods.forEach { food ->

            food.allergens.forEach { allergen ->
                val ingredientWithPossibleAllergen = allergenList[allergen]

                if (ingredientWithPossibleAllergen == null) {
                    allergenList[allergen] = food.ingredients
                } else {
                    allergenList[allergen] = food.ingredients.filter {
                        ing -> ingredientWithPossibleAllergen.contains(ing)
                    }
                }
            }
        }
        return allergenList
    }

    private fun getIngredientsWithoutAllergen(foods: List<Food>): List<String> {
        val allergenList = getAllergenWithIngredients(foods)

        val ingredientWithPossibleAllergen = mutableListOf<String>()
        allergenList.forEach {
            ingredientWithPossibleAllergen += it.value
        }

        val foodWithoutAllergen = mutableListOf<String>()
        foods.forEach { food ->
            foodWithoutAllergen += food.ingredients.filter { it ->
                !ingredientWithPossibleAllergen.contains(it)
            }
        }
        return foodWithoutAllergen
    }

    private fun getInput(context: Context, filename: String): List<Food> {
        val ret = mutableListOf<Food>()
        context.assets.open(filename).bufferedReader().use {
            it.forEachLine { line ->
                ret.add(Food(line))
            }
        }

        return ret
    }

    private class Food(data: String) {

        val allergens: List<String>
        val ingredients: List<String>

        init {
            val ingredients = mutableListOf<String>()
            val allergens = mutableListOf<String>()

            val parts = data.split(' ')
            var foundAllergens = false
            for (i in parts.indices) {
                val part = parts[i]
                if (part.startsWith('(')) {
                    foundAllergens = true
                } else {
                    if (foundAllergens) {
                        allergens.add(part.replace(")", "").replace(",", "").trim())
                    } else {
                        ingredients.add(part)
                    }
                }
            }

            this.ingredients = ingredients
            this.allergens = allergens
        }
    }
}