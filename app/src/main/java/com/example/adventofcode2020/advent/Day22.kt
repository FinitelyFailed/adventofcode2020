package com.example.adventofcode2020.advent

import android.content.Context
import java.util.*

class Day22: BaseDay {
    override fun testAnswer01(context: Context): Boolean {
        val combat = getInput(context, "day22_test")

        val winningPlayer = combat.run()

        val score = winningPlayer.getScore()
        return score == 306L
    }

    override fun testAnswer02(context: Context): Boolean {
        val combatRecursion = getInput(context, "day22_test_2", true)

        //val winningPlayerRecursion =
        combatRecursion.run()

        //val scoreRecursion = winningPlayerRecursion.getScore()

        val combat = getInput(context, "day22_test", true)

        val winningPlayer = combat.run()

        val score = winningPlayer.getScore()
        return score == 291L
    }

    override fun getAnswer01(context: Context): String {
        val combat = getInput(context, "day22")

        val winningPlayer = combat.run()

        val score = winningPlayer.getScore()
        return score.toString()
    }

    override fun getAnswer02(context: Context): String {
        val combat = getInput(context, "day22", true)

        val winningPlayer = combat.run()

        val score = winningPlayer.getScore()
        return score.toString()
    }

    private fun getInput(context: Context, filename: String, recursive: Boolean = false): BaseCombat {
        val players = mutableListOf<Player>()
        context.assets.open(filename).bufferedReader().use {
            var playerName = ""
            var startCards = mutableListOf<Int>()
            it.forEachLine { line ->
                when {
                    line.contains("Player") -> {
                        playerName = line.substring(0, line.lastIndex)
                    }
                    line.isBlank() -> {
                        players.add(Player(playerName, startCards))
                        playerName = ""
                        startCards = mutableListOf()
                    }
                    else -> {
                        startCards.add(line.toInt())
                    }
                }
            }

            if (startCards.isNotEmpty()) {
                players.add(Player(playerName, startCards))
            }
        }

        return if (recursive) {
            RecursiveCombat(players)
        } else {
            Combat(players)
        }
    }

    private abstract class BaseCombat(val players: List<Player>) {
        abstract fun run(): Player
    }

    private class Combat(players: List<Player>): BaseCombat(players) {

        var turn = 0
            private set

        // Run until a player wins, will return winner.
        override fun run(): Player {
            var cardPlayerMap = sortedMapOf<Int, Player>()
            while (true) {
                turn++

                val playersInTheGame = players.filter { p -> !p.hasLost() }
                if (playersInTheGame.size == 1) {
                    return playersInTheGame.first()
                }

                for (i in players.indices) {
                    val player = players[i]

                    if (!player.hasLost()) {
                        val card = player.getTopMostCard()
                        cardPlayerMap[card] = player
                    }
                }

                val cards = cardPlayerMap.keys.reversed()
                cardPlayerMap[cards.first()]!!.insertCards(cards)

                cardPlayerMap = sortedMapOf<Int, Player>()
            }
        }
    }

    private class RecursiveCombat(players: List<Player>): BaseCombat(players) {

        // Run until a player wins, will return winner.
        override fun run(): Player {
            val winner = runInner(players)

            //Log.d(TAG, "_\n== Post-game results ==")
            //players.forEach {
                //Log.d(TAG, "${it.name}'s deck: ${it.cards.joinToString(", ")}")
            //}


            return winner
        }

        fun runInner(players: List<Player>, gameNumber: Int = 1): Player {
            var round = 0
            var nextSubGameNumber = gameNumber + 1
            while (true) {
                round++

                val winner = getWinner(players)
                if (winner != null) {
                    //Log.d(TAG, "The winner of Game $gameNumber is ${winner.name}\n ")
                    return winner
                }

                players.forEach { player ->
                    player.addCardsToHistory()
                }

                //Log.d(TAG, "_\n")

                val cardPlayerMap = drawACardFromEachPlayer(players)

                //Log.d(TAG, "_\n -- Round $round (Game $gameNumber) --")
                cardPlayerMap.forEach {
                    //Log.d(TAG, "${it.value.name}'s deck: ${it.value.cards.joinToString(", ")}")
                    //Log.d(TAG, "${it.value.name} plays: ${it.key}")
                }

                if (shouldPlaySubGame(cardPlayerMap)) {

                    //Log.d(TAG, "Playing a sub-game to determine the winner...")
                    val playerCopies = mutableListOf<Player>()
                    cardPlayerMap.forEach {
                        playerCopies.add(it.value.copy(it.key))
                    }

                    val subWinner = runInner(playerCopies, nextSubGameNumber)
                    nextSubGameNumber++
                    //Log.d(TAG, "..anyway, back to game $gameNumber.")

                    for (key in cardPlayerMap.keys) {
                        if (cardPlayerMap[key]!!.name == subWinner.name) {
                            val roundWinner = cardPlayerMap[key]!!
                            val cards = listOf(key) + cardPlayerMap.keys.filter { it -> it != key }
                            roundWinner.insertCards(cards)

                            //Log.d(TAG, "${roundWinner.name} wins round $round of game $gameNumber")
                            break
                        }
                    }

                } else {
                    val cards = cardPlayerMap.keys.reversed()
                    val roundWinner = cardPlayerMap[cards.first()]!!
                    roundWinner.insertCards(cards)

                    //Log.d(TAG, "${roundWinner.name} wins round $round of game $gameNumber")
                }
            }
        }

        private fun getWinner(players: List<Player>): Player? {
            players.forEach { player ->
                if (player.currentCardsExistsInHistory()) {
                    return players.find { p -> p.name == "Player 1" }
                }
            }

            val playersInTheGame = players.filter { p -> !p.hasLost() }
            if (playersInTheGame.size == 1) {
                return playersInTheGame.first()
            }
            return null
        }

        private fun drawACardFromEachPlayer(players: List<Player>): SortedMap<Int, Player> {
            val cardPlayerMap = sortedMapOf<Int, Player>()
            for (i in players.indices) {
                val player = players[i]

                if (!player.hasLost()) {
                    val card = player.getTopMostCard()
                    cardPlayerMap[card] = player
                }
            }
            return cardPlayerMap
        }

        private fun shouldPlaySubGame(cardPlayerMap: SortedMap<Int, Player>): Boolean {
            cardPlayerMap.forEach { it ->
                if (it.key > it.value.cards.size) {
                    return false
                }
            }
            return true
        }

        companion object {
            private const val TAG: String = "Recursive Combat"
        }
    }

    private class Player(val name: String, startCards: List<Int>) {
        val cards: MutableList<Int> = startCards.toMutableList()

        private val cardsHistory = sortedMapOf<Int, MutableList<List<Int>>>()

        fun hasLost() = cards.isEmpty()

        fun currentCardsExistsInHistory(): Boolean {
            val cardsList = cardsHistory[cards.size]
            if (cardsList != null) {
                if (cardsList.contains(cards)) {
                    return true
                }
            }
            return false
        }

        fun getTopMostCard(): Int {
            return cards.removeAt(0)
        }

        fun insertCards(cardsToInsert: List<Int>) {
            cardsToInsert.forEach { card ->
                cards.add(card)
            }
        }

        fun getScore(): Long {
            var ret = 0L

            for (i in cards.indices) {
                ret += cards[cards.lastIndex - i] * (i + 1)
            }

            return ret
        }

        fun copy(numberOfCardsToCopy: Int): Player {
            val copyOfCards = List(numberOfCardsToCopy) { it -> cards[it] }
            return Player(name, copyOfCards)
        }

        fun addCardsToHistory() {
            val copyOfCards = List(cards.size) { it -> cards[it] }
            val cardsList = cardsHistory[cards.size]
            if (cardsList == null) {
                cardsHistory[cards.size] = mutableListOf(copyOfCards)
            } else {
                cardsHistory[cards.size]!!.add(copyOfCards)
            }
        }
    }
}